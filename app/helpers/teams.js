import { isArray, uniq } from 'lodash'

/*
  GLOBAL TEAMS
*/

const getAllEditors = globalTeams =>
  getGlobalTeamMembersByType(globalTeams, 'editors')

const getAllScienceOfficers = globalTeams =>
  getGlobalTeamMembersByType(globalTeams, 'scienceOfficers')

const getGlobalTeamMembersByType = (globalTeams, type) => {
  const globalTeam = getOneTeamByType(globalTeams, type)
  return getTeamMembers(globalTeam)
}

// Get all users that are not in the global teams
const getRegularUsers = (users, globalTeams) => {
  if (!users || !globalTeams) return null

  let allGlobalIds = []
  globalTeams.forEach(team => {
    team.members.forEach(member => {
      allGlobalIds.push(member.id)
    })
  })
  allGlobalIds = uniq(allGlobalIds)

  return users.filter(user => !user.admin && !allGlobalIds.includes(user.id))
}

/*
  PER ARTICLE TEAMS
*/

const getEditorTeamForArticle = teamsForArticle =>
  getOneTeamByType(teamsForArticle, 'editor')

const getEditorTeamId = teamsForArticle => {
  const editorTeam = getEditorTeamForArticle(teamsForArticle)
  if (!editorTeam) return null
  return editorTeam.id
}

const getScienceOfficerTeamForArticle = teamsForArticle =>
  getOneTeamByType(teamsForArticle, 'scienceOfficer')

/*
  GENERIC HELPERS
*/

const getFirstMemberOfTeam = team => {
  const members = getTeamMembers(team)
  if (!members) return null
  return members[0]
}

const getTeamMembers = team => {
  if (!team) return null
  const { members } = team

  if (!members || !isArray(members)) return null
  return members
}

const getOneTeamByType = (teams, type) =>
  teams && teams.find(t => t.role === type)

/*
  EXPORT
*/

export {
  getAllEditors,
  getAllScienceOfficers,
  getGlobalTeamMembersByType,
  getEditorTeamForArticle,
  getEditorTeamId,
  getRegularUsers,
  getScienceOfficerTeamForArticle,
  getFirstMemberOfTeam,
  getTeamMembers,
  getOneTeamByType,
}
