import config from 'config'

const { baseUrl } = config['pubsweet-client']
const apiUrl = `${baseUrl}/api/pubmed`

const get = async (endpoint, value) => {
  const url = `${apiUrl}/${endpoint}?id=${value}`
  return fetch(url)
}

const validate = async (endpoint, value) => {
  const { id } = value
  const url = `${apiUrl}/validate/${endpoint}?id=${id}`
  return fetch(url)
}

const getReference = value => get('reference', value)

const validateReference = value => validate('reference', value)

export { getReference, validateReference }
