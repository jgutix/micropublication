const getInvitedReviewersTeam = versionTeams => {
  const reviewerTeam = versionTeams.find(t => t.role === 'reviewer')
  const invitedReviewers = reviewerTeam.members.filter(member =>
    ['invited', 'acceptedInvitation', 'rejectedInvitation'].includes(
      member.status,
    ),
  )

  return invitedReviewers
}

const getManuscriptTeam = manuscriptTeams => {
  const manuscriptEditors = membersOfTeam(manuscriptTeams, 'editor')
  const manuscriptEditor = manuscriptEditors && manuscriptEditors[0]

  const manuscriptSOs = membersOfTeam(manuscriptTeams, 'scienceOfficer')
  const manuscriptSO = manuscriptSOs && manuscriptSOs[0]

  return {
    editor: manuscriptEditor,
    scienceOfficer: manuscriptSO,
  }
}

const getReviewerCounts = versionTeams => {
  const reviewerTeam = getReviewerTeam(versionTeams)
  const invitedReviewers = getInvitedReviewersTeam(versionTeams)
  const acceptedReviewers = reviewerTeam.members.filter(
    member => member.status === 'acceptedInvitation',
  )
  const rejectedReviewers = reviewerTeam.members.filter(
    member => member.status === 'rejectedInvitation',
  )

  return {
    invited: invitedReviewers.length,
    accepted: acceptedReviewers.length,
    rejected: rejectedReviewers.length,
  }
}

const getReviewerTeam = versionTeams =>
  versionTeams.find(t => t.role === 'reviewer')

const getSubmittingAuthor = authors => {
  const submittingAuthor = authors.find(author => author.submittingAuthor)

  return {
    authorEmail: submittingAuthor.email,
    authorName: `${submittingAuthor.firstName} ${submittingAuthor.lastName}`,
  }
}

const membersOfTeam = (teams, role) => {
  if (!teams || !role) return null

  const team = teams.find(t => t.role === role)
  if (!team) return null

  return team.members.map(member => ({
    id: member.user.id,
    displayName: member.user.displayName,
  }))
}

const transformChatMessages = messages =>
  messages &&
  messages.map(message => ({
    id: message.id,
    content: message.content,
    displayName: message.user.displayName,
  }))

const transformReviews = reviews =>
  reviews.map(review => ({
    askedToSeeRevision: review.askedToSeeRevision,
    content: review.content,
    openAcknowledgement: review.openAcknowledgement,
    pending: review.status.pending,
    recommendation: review.recommendation,
    reviewerId: review.reviewer.id,
    reviewerName: review.reviewer.displayName,
  }))

export {
  getInvitedReviewersTeam,
  getManuscriptTeam,
  getReviewerCounts,
  getReviewerTeam,
  getSubmittingAuthor,
  membersOfTeam,
  transformChatMessages,
  transformReviews,
}
