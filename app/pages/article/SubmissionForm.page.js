import React from 'react'
import PropTypes from 'prop-types'
import { useQuery, useMutation } from '@apollo/react-hooks'
import {
  cloneDeep,
  isEmpty,
  isEqual,
  keys,
  omit,
  values as lodashValues,
  union,
} from 'lodash'
import { v4 as uuid } from 'uuid'

import {
  MANUSCRIPT_STATUS,
  MANUSCRIPT_SUBMISSION_FORM,
  SAVE_FORM,
  SUBMIT_MANUSCRIPT,
  UPLOAD_FILE,
} from '../../graphql'

import { Loader } from '../../../ui'
import SubmissionForm from '../../components/SubmissionForm'

const dataToFormValues = data => {
  const vals = cloneDeep(data)
  const { authors, image, references } = data

  if (authors) {
    const modAuthors = authors.map(item => {
      const modAuthor = cloneDeep(item)
      modAuthor.id = uuid()
      if (isEmpty(modAuthor.affiliations)) {
        modAuthor.affiliations = ['']
      }
      if (!modAuthor.email) modAuthor.email = ''
      return omit(modAuthor, '__typename')
    })

    vals.authors = modAuthors
  }

  if (references) {
    vals.references = references.map(item => {
      const modReference = cloneDeep(item)
      modReference.id = uuid()
      return modReference
    })
  }

  delete vals.__typename // eslint-disable-line no-underscore-dangle

  // eslint-disable-next-line no-underscore-dangle
  if (image && image.__typename) {
    vals.image = omit(image, '__typename')
  }

  return vals
}

/* eslint-disable no-underscore-dangle, no-param-reassign */
// TODO -- write data cleanup functions (eg. remove __typename)
const formValuesToData = values => {
  const data = cloneDeep(values)
  const { authors, status, references } = data

  if (authors) {
    data.authors = union([], authors)
    // author.submittingAuthor = true
    // data.authors.push(author)

    data.authors = data.authors
      .map(item => {
        const modAuthor = cloneDeep(item)
        delete modAuthor.id
        delete modAuthor.__typename
        // if (!modAuthor.submittingAuthor) modAuthor.submittingAuthor = null
        if (!modAuthor.email) modAuthor.email = null
        else modAuthor.email = modAuthor.email.trim()

        modAuthor.affiliations = modAuthor.affiliations.filter(
          aff => aff !== '',
        )
        return modAuthor
      })
      .filter(item => {
        if (
          isEmpty(item.firstName) &&
          (isEmpty(item.credit) || isEqual(item.credit, [''])) &&
          isEmpty(item.affiliations) &&
          !item.submittingAuthor &&
          isEmpty(item.orcid)
        ) {
          return false
        }

        return true
      })
  }

  if (status) {
    delete status.__typename
    if (status.decision) delete status.decision.__typename
    if (status.scienceOfficer) delete status.scienceOfficer.__typename
    if (status.submission) delete status.submission.__typename
  }

  if (references) {
    data.references = references
      .map(item => {
        const modReference = cloneDeep(item)
        delete modReference.__typename
        delete modReference.id
        return modReference
      })
      .filter(item => {
        if (
          isEmpty(item.pubmedId) &&
          isEmpty(item.doi) &&
          (isEmpty(item.reference) || isEqual(item.reference, '<p></p>'))
        ) {
          return false
        }
        return true
      })
  }

  // delete data.author
  // delete data.coAuthors

  if (data.laboratory) delete data.laboratory.__typename

  if (data.geneExpression) {
    delete data.geneExpression.__typename

    if (data.geneExpression.dnaSequence) {
      data.geneExpression.dnaSequence.forEach(item => {
        delete item.__typename
        delete item.id
      })
    }

    if (data.geneExpression.transgeneUsed) {
      data.geneExpression.transgeneUsed.forEach(item => {
        delete item.__typename
        delete item.id
      })
    }

    if (data.geneExpression.variation) {
      delete data.geneExpression.variation.__typename
    }

    if (data.geneExpression.utr) {
      delete data.geneExpression.utr.__typename
    }

    if (data.geneExpression.species) {
      delete data.geneExpression.species.__typename
    }

    if (data.geneExpression.reporter) {
      delete data.geneExpression.reporter.__typename
    }

    if (data.geneExpression.integratedBy) {
      delete data.geneExpression.integratedBy.__typename
    }

    if (data.geneExpression.fusionType) {
      delete data.geneExpression.fusionType.__typename
    }

    if (data.geneExpression.expressionPattern) {
      delete data.geneExpression.expressionPattern.__typename
    }

    if (data.geneExpression.backboneVector) {
      delete data.geneExpression.backboneVector.__typename
    }

    if (data.geneExpression.observeExpression) {
      delete data.geneExpression.observeExpression.__typename

      keys(data.geneExpression.observeExpression).forEach(key => {
        data.geneExpression.observeExpression[key].forEach(item => {
          delete item.__typename
          if (!item.id) item.id = uuid()

          lodashValues(item).forEach(entry => {
            delete entry.__typename
            delete entry.id
          })
        })
      })
    }
  }

  const autocompleteKeys = keys(data).filter(key => {
    const match = key.match(/react-autowhatever*/)
    return match !== null
  })

  autocompleteKeys.forEach(key => delete data[key])

  if (data.image) delete data.image.__typename
  if (data.suggestedReviewer) delete data.suggestedReviewer.__typename

  delete data.__typename

  delete data.active
  delete data.dataType
  delete data.submitted
  delete data.created
  delete data.isApprovedByScienceOfficer

  return data
}
/* eslint-enable no-underscore-dangle, no-param-reassign */

const SubmissionFormPage = props => {
  const { manuscriptId, submissionType } = props

  /**
   * Fetch data
   */
  const { data, loading } = useQuery(MANUSCRIPT_SUBMISSION_FORM, {
    variables: {
      id: manuscriptId,
    },
  })

  let formData
  if (data && data.manuscript) {
    formData = dataToFormValues(data.manuscript.versions[0])
  }

  /**
   * Autosave form
   */
  const [saveForm] = useMutation(SAVE_FORM, {
    refetchQueries: [
      {
        query: MANUSCRIPT_SUBMISSION_FORM,
        variables: { id: manuscriptId },
      },
      // ALSO DASHBOARD MANUSCRIPTS?
      // BECAUSE OF TITLE
    ],
  })

  const saveFormFn = saveData =>
    saveForm({
      variables: {
        input: formValuesToData(saveData),
      },
    })

  /**
   * Upload image
   */
  const [uploadFile] = useMutation(UPLOAD_FILE)
  const upload = file => uploadFile({ variables: { file } })

  /**
   * Submit manuscript
   */
  const [submitManuscript] = useMutation(SUBMIT_MANUSCRIPT, {
    refetchQueries: [
      {
        query: MANUSCRIPT_STATUS,
        variables: { id: manuscriptId },
      },
      // {
      //   query: MANUSCRIPT_SUBMISSION_FORM,
      //   variables: { id: manuscriptId },
      // },
    ],
  })

  const submitManuscriptFn = values =>
    submitManuscript({
      variables: {
        input: formValuesToData(values),
      },
    })

  /**
   * UI
   */
  if (loading) return <Loader />

  return (
    <SubmissionForm
      data={formData}
      saveForm={saveFormFn}
      submissionType={submissionType}
      submitManuscript={submitManuscriptFn}
      upload={upload}
    />
  )
}

SubmissionFormPage.propTypes = {
  manuscriptId: PropTypes.string.isRequired,
  submissionType: PropTypes.oneOf(['initial', 'full', 'revision']).isRequired,
}

export default SubmissionFormPage
