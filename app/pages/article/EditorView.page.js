import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useQuery, useMutation } from '@apollo/react-hooks'

import {
  EDITOR_PANEL,
  FULL_PREVIEW,
  REINVITE_REVIEWER,
  SEND_CHAT,
  SET_DATA_TYPE,
  SUBMIT_DECISION,
  UPDATE_MANUSCRIPT_METADATA,
  UPDATE_MANUSCRIPT_TEAMS,
} from '../../graphql'

import {
  ChatModal,
  DataTypeConfirmation,
  DateParser,
  EditorPanel,
  ManuscriptTeamManager,
  SyncedTabs,
} from '../../../ui'

import { ArticlePreview } from '../../components/ui'

import { exportManuscriptToHTML } from '../../fetch/exportManuscript'
import transform from '../_helpers/transformEditorPanelData'
import { transformChatMessages } from '../_helpers/common'

/* eslint-disable-next-line react/prop-types */
const Label = ({ created, index }) => (
  <DateParser dateFormat="MM.DD.YY HH:mm" timestamp={new Date(Number(created))}>
    {timestamp => (
      <span>
        {index === 0
          ? `Original: ${timestamp}`
          : `Revision ${index}: ${timestamp}`}
      </span>
    )}
  </DateParser>
)

const EditorView = props => {
  const { manuscriptId } = props

  /**
   * Handle modals with state
   */
  const [selectedDataType, setSelectedDataType] = useState(null)
  const [showDataTypeConfirm, setShowDataTypeConfirm] = useState(false)

  const [chatModalThreadId, setChatModalThreadId] = useState(null)
  const [showChatModal, setShowChatModal] = useState(false)
  const [showTeamManager, setShowTeamManager] = useState(false)

  const onClickSetDataType = selected => {
    setSelectedDataType(selected)
    setShowDataTypeConfirm(true)
  }

  let chatData // will be populated further down
  const onClickReviewerChat = reviewerId => {
    const selectedChat = chatData.find(item => item.reviewerId === reviewerId)
    setChatModalThreadId(selectedChat.id)
    setShowChatModal(true)
  }

  /**
   * Queries & mutations
   */

  const { data: previewData, loading: previewLoading } = useQuery(
    FULL_PREVIEW,
    {
      variables: { id: manuscriptId },
    },
  )

  const { data: editorData, loading: editorLoading } = useQuery(EDITOR_PANEL, {
    variables: { id: manuscriptId },
  })

  const queryRefetch = {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
    ],
  }

  const [reinviteReviewerMutation] = useMutation(
    REINVITE_REVIEWER,
    queryRefetch,
  )
  const [sendChatMutation] = useMutation(SEND_CHAT, {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
      {
        query: FULL_PREVIEW,
        variables: { id: manuscriptId },
      },
    ],
  })
  const [setDataTypeMutation] = useMutation(SET_DATA_TYPE, queryRefetch)
  const [submitDecisionMutation] = useMutation(SUBMIT_DECISION, {
    refetchQueries: [
      {
        query: EDITOR_PANEL,
        variables: { id: manuscriptId },
      },
      {
        query: FULL_PREVIEW,
        variables: { id: manuscriptId },
      },
    ],
  })
  const [updateManuscriptTeamsMutation] = useMutation(
    UPDATE_MANUSCRIPT_TEAMS,
    queryRefetch,
  )
  const [updateMetadataMutation] = useMutation(
    UPDATE_MANUSCRIPT_METADATA,
    queryRefetch,
  )

  /**
   * Left side: preview
   */

  let leftSections

  if (
    previewData &&
    previewData.manuscript &&
    previewData.manuscript.versions
  ) {
    // there is only one chat here, the author chat
    const authorChat = previewData.manuscript.chatThreads[0]

    const authorChatMessages = transformChatMessages(authorChat.messages)

    const sendAuthorChatMessage = content =>
      sendChatMutation({
        variables: {
          input: {
            chatThreadId: authorChat.id,
            content,
          },
        },
      })

    const filteredVersions = previewData.manuscript.versions.filter(
      v =>
        (!v.submitted &&
          previewData.manuscript.isInitiallySubmitted &&
          !previewData.manuscript.isDataTypeSelected) ||
        v.submitted,
    )

    leftSections = filteredVersions.map((version, index) => ({
      key: version.id,
      label: <Label created={version.created} index={index} />,
      content: (
        <ArticlePreview
          article={version}
          authorChatMessages={authorChatMessages}
          exportManuscript={exportManuscriptToHTML}
          isEditor
          manuscriptId={manuscriptId}
          previousVersion={filteredVersions[index - 1]}
          sendAuthorChatMessage={sendAuthorChatMessage}
          showAdditionalData
          showHeader={false}
        />
      ),
    }))
  }

  /**
   * Right side: editor panel
   */

  let rightSections
  let editors, scienceOfficers /* curators */
  let assignedEditor, assignedScienceOfficer /* assignedCurator */
  let sendModalChatMessage, setDataType, updateManuscriptTeams, updateMetadata

  if (!editorLoading && editorData) {
    const transformed = transform(editorData, {
      reinviteReviewerMutation,
      sendChatMutation,
      setDataTypeMutation,
      submitDecisionMutation,
      updateManuscriptTeamsMutation,
      updateMetadataMutation,
    })

    ;({
      assignedEditor,
      assignedScienceOfficer,

      editors,
      scienceOfficers,
      // curators,

      chatData,

      setDataType,
      updateManuscriptTeams,
      updateMetadata,
    } = transformed)

    const {
      doi,
      reinviteReviewer: reinviteReviewerFn,
      sendChatMessage,
      submitDecision: submitDecisionFn,
      versions,
    } = transformed

    const scienceOfficerChat = chatData.find(
      thread => thread.chatType === 'scienceOfficer',
    )
    const scienceOfficerChatMessages = scienceOfficerChat.messages

    const sendScienceOfficerChatMessage = content =>
      sendChatMessage(content, scienceOfficerChat.id)

    sendModalChatMessage = content =>
      sendChatMessage(content, chatModalThreadId)

    rightSections = versions.map((version, index) => {
      const {
        authorEmail,
        authorName,
        created,
        dataType,
        decision,
        decisionLetter,
        id,
        latest,

        invitedReviewersCount,
        acceptedReviewersCount,
        rejectedReviewersCount,

        reviews,
        previousReviewers,
      } = version

      const editorName = assignedEditor && assignedEditor.displayName
      const scienceOfficerName =
        assignedScienceOfficer && assignedScienceOfficer.displayName

      const reinviteReviewer = reviewerId =>
        reinviteReviewerFn(version.id, reviewerId)
      const submitDecision = input => submitDecisionFn(version.id, input)

      return {
        key: id,
        label: <Label created={created} index={index} />,
        content: (
          <EditorPanel
            acceptedReviewersCount={acceptedReviewersCount}
            authorEmail={authorEmail}
            authorName={authorName}
            dataType={dataType}
            decision={decision}
            decisionLetter={decisionLetter}
            decisionSubmitted={!!decision}
            doi={doi}
            editorName={editorName}
            invitedReviewersCount={invitedReviewersCount}
            key={version.id}
            onClickReviewerChat={onClickReviewerChat}
            onClickReviewerReinvite={reinviteReviewer}
            onClickSetDataType={onClickSetDataType}
            onClickTeamManager={() => setShowTeamManager(true)}
            rejectedReviewersCount={rejectedReviewersCount}
            reviewersFromPreviousVersions={previousReviewers}
            reviewersPageUrl={`/assign-reviewers/${manuscriptId}`}
            reviews={reviews}
            scienceOfficerChatMessages={scienceOfficerChatMessages}
            scienceOfficerName={scienceOfficerName}
            sendScienceOfficerChatMessage={sendScienceOfficerChatMessage}
            showDataType={latest}
            showDataTypeSelection
            showDecision
            showInfo={latest}
            showManageReviewers={latest}
            showMetadata={latest}
            showPreviousReviewers={latest && versions.length > 1}
            showReviewersChat
            showReviews
            showScienceOfficerChat={latest}
            showTeamManager
            // startChatExpanded
            startDataTypeExpanded={!dataType}
            // startDecisionExpanded
            // startMetadataExpanded
            // startReviewsExpanded
            submitDecision={submitDecision}
            updateMetadata={updateMetadata}
          />
        ),
      }
    })
  }

  /**
   * Render
   */

  return (
    <>
      <SyncedTabs
        leftHeader="Article Preview"
        leftLoading={previewLoading}
        leftSections={leftSections}
        rightHeader="Editor Panel"
        rightLoading={editorLoading}
        rightSections={rightSections}
      />

      {/* Modals */}

      {!editorLoading && (
        <ManuscriptTeamManager
          assignedEditor={assignedEditor}
          assignedScienceOfficer={assignedScienceOfficer}
          // curators={curators}
          editors={editors}
          // isOpen
          isOpen={showTeamManager}
          onRequestClose={() => setShowTeamManager(false)}
          scienceOfficers={scienceOfficers}
          updateTeams={updateManuscriptTeams}
        />
      )}

      <DataTypeConfirmation
        dataTypeLabel={selectedDataType && selectedDataType.label}
        isOpen={showDataTypeConfirm}
        onConfirm={() => {
          setDataType(selectedDataType.value).then(() => {
            setShowDataTypeConfirm(false)
          })
        }}
        onRequestClose={() => setShowDataTypeConfirm(false)}
      />

      {chatModalThreadId && (
        <ChatModal
          isOpen={showChatModal}
          messages={
            chatData.find(thread => thread.id === chatModalThreadId).messages
          }
          onRequestClose={() => setShowChatModal(false)}
          sendMessage={sendModalChatMessage}
        />
      )}
    </>
  )
}

EditorView.propTypes = {
  manuscriptId: PropTypes.bool.isRequired,
}

export default EditorView
