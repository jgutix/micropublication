const request = require('request')
const { head } = require('lodash')

const baseUrl = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/'

// const apiKey = config.get('pubmed.apiKey')

const makeRequest = (req, res) => {
  const reqUrl = `${baseUrl}esummary.fcgi`
  request(
    {
      qs: {
        db: 'pubmed',
        id: req.query.id,
        retmode: 'json',
      },
      url: reqUrl,
    },
    (error, response, body) => {
      // eslint-disable-next-line no-console
      if (error) return console.error(error)
      return res.send({ reference: makeReference(JSON.parse(body)) })
    },
  )
}

const makeValidateRequest = (req, res) => {
  const validateUrl = `${baseUrl}esearch.fcgi`
  request(
    {
      qs: {
        db: 'pubmed',
        term: `${req.query.id}[UID]`,
        retmode: 'json',
      },
      url: validateUrl,
    },
    (error, response, body) => {
      // eslint-disable-next-line no-console
      if (error) return console.error(error)
      const json = JSON.parse(body)
      const result = json.esearchresult.count === '1'
      return res.send({ data: { result } })
    },
  )
}

const makeReference = data => {
  if (data.result.uids.length === 0) {
    return ''
  }
  const uid = data.result[head(data.result.uids)]
  const authors = uid.authors.map(author => author.name).join(', ')
  const year = head(uid.pubdate.split(' '))
  const { title, source, volume, pages } = uid

  return `${authors}. ${year}. ${title} ${source} ${volume}: ${pages}.`
}

const PubMedApi = app => {
  /**
   * GET ENDPOINTS
   */

  app.get('/api/pubmed/fetch/reference', (req, res) => {
    makeRequest(req, res)
  })

  /**
   * VALIDATION ENDPOINTS
   */

  app.get('/api/pubmed/validate/reference', (req, res) => {
    makeValidateRequest(req, res)
  })
}

module.exports = PubMedApi
