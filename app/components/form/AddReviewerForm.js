/* eslint-disable react/prop-types */

import React from 'react'
import * as yup from 'yup'

import Form from './Form'

const initialValues = {
  email: '',
  givenNames: '',
  surname: '',
}

const validations = yup.object().shape({
  email: yup
    .string()
    .required('Email is required')
    .email('Invalid email address'),
  givenNames: yup.string().required('Given names are required'),
  surname: yup.string().required('Surname is required'),
})

const AddReviewerForm = props => {
  const { addExternalReviewer } = props

  const handleSubmit = (formValues, formikBag) => {
    const { email, givenNames, surname } = formValues

    const input = {
      email,
      givenNames,
      surname,
    }

    addExternalReviewer(input).then(() => formikBag.resetForm())
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
      {...props}
    />
  )
}

export default AddReviewerForm
