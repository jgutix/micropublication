/* eslint-disable react/prop-types */

import React from 'react'
import { isNull, isUndefined } from 'lodash'

import { isTruthy } from '../../helpers/generic'
import { Form } from './index'

const ScienceOfficerApprovalForm = props => {
  const { approved, article, setSOApproval, ...otherProps } = props

  let approvedValue
  if (!isUndefined(approved) && !isNull(approved))
    approvedValue = approved.toString()

  const initialValues = {
    approve: approvedValue,
  }

  const handleSubmit = (values, formikBag) => {
    const { approve } = values
    setSOApproval(isTruthy(approve))
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      {...otherProps}
    />
  )
}

export default ScienceOfficerApprovalForm
