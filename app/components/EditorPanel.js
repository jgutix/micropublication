/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
// import { first, last } from 'lodash'

import { th } from '@pubsweet/ui-toolkit'

import { grid } from '../../ui/src/_helpers'
// import { shouldShowAssignReviewersLink } from '../helpers/status'

import {
  DecisionSection,
  DiscussSection,
  EditorPanelMetadata,
  EditorPanelRibbon,
  PanelInfo,
  ReviewerInfo,
  ScienceOfficerSection,
} from './ui'

const Wrapper = styled.div`
  /* padding-right: calc(${th('gridUnit')} * 2); */
  padding-top: ${grid(1)};
`

const Decision = ({ approved, version, submitDecision }) => (
  <DecisionSection
    approvedByScienceOfficer={approved}
    data={version}
    submitDecision={submitDecision}
  />
)

const Tab = props => {
  const {
    articleId, // done
    authorChatMessages, // to delete
    currentUser, // to delete
    doi, // done
    editor,
    isAdmin,
    isEditor,
    isLastSubmittedVersion,
    isScienceOfficer,
    reinviteReviewer,
    latest, // done
    reviewerChatThreads,
    scienceOfficer,
    scienceOfficerChatMessages,
    sendAuthorChatMessage,
    sendReviewerChatMessage,
    sendSOChatMessage,
    setSOApproval,
    showReviewerAssignmentLink,
    submitDecision,
    updateMetadata,
    version,
  } = props

  console.log(version)

  const {
    authors,
    decision,
    editorSuggestedReviewers,
    isApprovedByScienceOfficer,
    previousReviewers,
    reviewerCounts,
    reviews,
  } = version

  const author = authors.find(a => a.submittingAuthor)
  const alreadyRejected = decision === 'reject'
  const approved = isApprovedByScienceOfficer === true
  const notApproved = isApprovedByScienceOfficer === false
  const scienceOfficerHasDecision = approved || notApproved

  const deriveRibbonStatus = () => {
    if (decision === 'accept') return 'accepted'
    if (decision === 'reject') return 'rejected'
    if (decision === 'revise') return 'revise'
    if (approved) return 'scienceOfficerApproved'
    if (notApproved) return 'scienceOfficerDeclined'
    if (!scienceOfficerHasDecision) return 'scienceOfficerPending'
    return null
  }

  return (
    <Wrapper>
      <EditorPanelRibbon type={deriveRibbonStatus()} />

      {latest && (
        <PanelInfo
          author={author}
          authorChatMessages={authorChatMessages}
          editor={editor}
          isEditor={isEditor}
          scienceOfficer={scienceOfficer}
          sendAuthorChatMessage={sendAuthorChatMessage}
        />
      )}

      {!alreadyRejected && !decision && (isScienceOfficer || isAdmin) && (
        <ScienceOfficerSection
          approved={isApprovedByScienceOfficer}
          editorSuggestedReviewers={editorSuggestedReviewers}
          setSOApproval={setSOApproval}
        />
      )}

      {latest && (
        <DiscussSection
          currentUser={currentUser}
          data={scienceOfficerChatMessages}
          sendChat={sendSOChatMessage}
        />
      )}

      <ReviewerInfo
        articleId={articleId}
        previousReviewers={previousReviewers}
        reinviteReviewer={reinviteReviewer}
        reviewerChatThreads={reviewerChatThreads}
        reviewerCounts={reviewerCounts}
        reviews={reviews}
        sendReviewerChatMessage={sendReviewerChatMessage}
        showReviewerAssignmentLink={
          showReviewerAssignmentLink && isLastSubmittedVersion
        }
      />

      {(decision || (!decision && (isEditor || isAdmin))) && (
        <Decision
          approved={isApprovedByScienceOfficer}
          submitDecision={submitDecision}
          version={version}
        />
      )}

      {latest && (
        <EditorPanelMetadata
          articleId={articleId}
          doi={doi}
          updateMetadata={updateMetadata}
        />
      )}
    </Wrapper>
  )
}

// const EditorPanel = props => {
//   const soChat = chatThreads.find(
//     thread => thread.chatType === 'scienceOfficer',
//   )
//   const soChatMessages = soChat.messages

//   const authorChat = chatThreads.find(thread => thread.chatType === 'author')
//   const authorChatMessages = authorChat.messages
//   const lastSubmittedVersion = last(versions.filter(v => v.submitted))
//   const firstSubmittedVersion = first(versions.filter(v => v.submitted))

//     content: (
//       <Tab
//         authorChatMessages={authorChatMessages}
//         isLastSubmittedVersion={version.id === lastSubmittedVersion.id}
//         reviewerChatThreads={reviewerChatThreads}
//         scienceOfficerChatMessages={soChatMessages}
//       />

export default Tab
