import gql from 'graphql-tag'

const LOGIN_USER = gql`
  mutation($input: LoginUserInput) {
    login(input: $input) {
      token
    }
  }
`

export default LOGIN_USER
