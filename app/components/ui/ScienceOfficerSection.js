/* eslint-disable react/prop-types */

import React, { Fragment } from 'react'
import styled, { withTheme } from 'styled-components'

import { Button } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import Accordion from './Accordion'
import RibbonFeedback from './RibbonFeedback'
import { ScienceOfficerApprovalForm } from '../form'
import { Radio } from '../formElements'

const makeOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Approve',
    value: 'true',
  },
  {
    color: theme.colorError,
    label: 'Do not approve',
    value: 'false',
  },
]

const Wrapper = styled.div`
  margin-bottom: calc(${th('gridUnit')} * 2);
  margin-left: calc(${th('gridUnit')} * 3);
`

const Header = styled.div`
  font-weight: bold;
  margin-top: ${th('gridUnit')};
`

const ScienceOfficerSection = props => {
  const { approved, editorSuggestedReviewers, theme, ...otherProps } = props
  const options = makeOptions(theme)

  const pending = approved !== true && approved !== false

  return (
    <Accordion label="Science Officer">
      <Wrapper>
        <RibbonFeedback
          keepSpaceOccupied={false}
          successMessage="Approval status successfully changed"
        >
          {notifyRibbon => (
            <Fragment>
              <Header>Reviewers suggested by Editor</Header>
              <div>
                {editorSuggestedReviewers.map(
                  (reviewer, i) =>
                    `${reviewer.user.displayName}${
                      i === editorSuggestedReviewers.length - 1 ? '' : ', '
                    }`,
                )}
              </div>

              <ScienceOfficerApprovalForm approved={approved} {...otherProps}>
                {formProps => (
                  <React.Fragment>
                    <Header>Approval</Header>
                    <Radio
                      inline
                      name="approve"
                      options={options}
                      {...formProps}
                    />

                    <Button
                      disabled={
                        !formProps.values.approve ||
                        String(approved) === formProps.values.approve
                      }
                      onClick={() => notifyRibbon(true)}
                      primary
                      type="submit"
                    >
                      {pending ? 'Submit' : 'Change'}
                    </Button>
                  </React.Fragment>
                )}
              </ScienceOfficerApprovalForm>
            </Fragment>
          )}
        </RibbonFeedback>
      </Wrapper>
    </Accordion>
  )
}

export default withTheme(ScienceOfficerSection)
