/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { clone } from 'lodash'

import { Action } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { Accordion, ReviewersNumbers, ReviewsPreview } from './index'
import PreviousReviewers from './PreviousReviewers'
import RibbonFeedback from './RibbonFeedback'
import { withCurrentUser } from '../../userContext'

const AssignReviewerLinkWrapper = styled.div`
  margin-top: ${th('gridUnit')};
`

const StyledAction = styled(Action)`
  line-height: unset;
`

const ContentWrapper = styled.div`
  margin-left: calc(${th('gridUnit')} * 3);

  > div:not(:first-child):not(:last-child) {
    margin-bottom: calc(${th('gridUnit')} * 2);
  }
`

const ReviewerInfo = props => {
  const {
    articleId,
    currentUser,
    previousReviewers,
    reinviteReviewer,
    // requestReviewerAttention,
    reviewerChatThreads,
    reviewerCounts,
    reviews,
    sendReviewerChatMessage,
    showReviewerAssignmentLink,
  } = props

  const reviewsWithChat = reviews.map(review => {
    const reviewWithChat = clone(review)

    reviewWithChat.chatThread = reviewerChatThreads.find(
      thread => thread.reviewerId === review.reviewer.id,
    )

    return reviewWithChat
  })

  const isAdmin = currentUser.admin
  const isEditor = currentUser.auth.isGlobalEditor
  const showPreviousReviewers =
    previousReviewers && previousReviewers.length > 0

  return (
    <Accordion label="Reviews">
      <ContentWrapper>
        <RibbonFeedback
          keepSpaceOccupied={false}
          successMessage="Reviewer has been successfully reinivited"
        >
          {notifyRibbon => {
            const reinvite = reviewerId =>
              reinviteReviewer(reviewerId).then(() => notifyRibbon(true))

            return (
              <>
                {(isEditor || isAdmin) && showReviewerAssignmentLink && (
                  <>
                    <AssignReviewerLinkWrapper>
                      <StyledAction to={`/assign-reviewers/${articleId}`}>
                        Go to Reviewer Assignment Page
                      </StyledAction>
                    </AssignReviewerLinkWrapper>

                    {showPreviousReviewers && (
                      <PreviousReviewers
                        reinviteReviewer={reinvite}
                        reviewers={previousReviewers}
                      />
                    )}
                  </>
                )}

                <ReviewersNumbers data={reviewerCounts} />

                <ReviewsPreview
                  isEditor={isEditor}
                  // manuscriptId={articleId}
                  // requestReviewerAttention={requestReviewerAttention}
                  reviews={reviewsWithChat}
                  sendReviewerChatMessage={sendReviewerChatMessage}
                />
              </>
            )
          }}
        </RibbonFeedback>
      </ContentWrapper>
    </Accordion>
  )
}

export default withCurrentUser(ReviewerInfo)
