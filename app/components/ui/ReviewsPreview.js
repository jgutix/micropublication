/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { Toggle, State } from 'react-powerplug'

import { List } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import {
  ChatModal,
  DiscreetButton,
  PanelSectionHeader,
  PanelTextEditor,
  RecommendationDot,
  Ribbon,
} from './index'

const Message = styled.div`
  color: ${th('colorTextPlaceholder')};
  margin-top: calc(${th('gridUnit')} * 2);
`

const Editor = styled(PanelTextEditor)`
  div[contenteditable] {
    border: 0;
  }
`

const ReviewHeadWrapper = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBorder')};
  display: flex;
  margin-bottom: ${th('gridUnit')};
`

const ReviewWrapper = styled.div`
  /* margin: ${th('gridUnit')} 0 calc(${th('gridUnit')} * 2) 0; */
  margin-bottom: calc(${th('gridUnit')} * 2);
`

const Pending = styled.div`
  align-self: flex-end;
  color: ${th('colorPrimary')};
  font-size: ${th('fontSizeBaseSmall')};
  font-style: italic;
  line-height: ${th('lineHeightBaseSmall')};
  margin-left: auto; /* pull right */
`

const StyledAcknowledgement = styled.div`
  align-self: flex-end;
  color: ${props =>
    props.open ? props.theme.colorSuccess : props.theme.colorError};
  font-size: ${th('fontSizeBaseSmall')};
  font-style: italic;
  font-weight: bold;
  line-height: ${th('lineHeightBaseSmall')};
  margin-left: auto; /* pull right */
`

const Recommendation = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  text-transform: uppercase;

  > span {
    color: ${props => {
      if (props.status === 'revise') return th('colorWarning')
      if (props.status === 'accept') return th('colorSuccess')
      if (props.status === 'reject') return th('colorError')
      return th('colorText')
    }};
  }
`

const Acknowledgement = ({ open }) => {
  const text = open ? 'open acknowledgement' : 'anonymous'
  return <StyledAcknowledgement open={open}>{text}</StyledAcknowledgement>
}

const showReviewerRevision = show =>
  show
    ? 'Reviewer would like to see revision'
    : 'Reviewer does not need to see revision'

const Review = props => {
  const {
    // articleVersionId,
    chatThread,
    content,
    isEditor,
    // onNotificationSent,
    openAcknowledgement,
    // requestReviewerAttention,
    recommendation,
    reviewer,
    sendReviewerChatMessage,
    status,
    askedToSeeRevision,
  } = props

  // const pingReviewer = () => {
  //   const data = { reviewerId: reviewer.id }

  //   requestReviewerAttention(data)
  //     .then(() => {
  //       onNotificationSent('success')
  //     })
  //     .catch(e => {
  //       onNotificationSent('error')
  //     })
  // }

  const sendChatMessage = messageContent =>
    sendReviewerChatMessage(messageContent, chatThread.id)

  return (
    <ReviewWrapper>
      <ReviewHeadWrapper>
        <RecommendationDot
          recommendation={status.pending ? null : recommendation}
        />

        {reviewer.displayName}

        {status.pending && <Pending>pending</Pending>}

        {!status.pending && <Acknowledgement open={openAcknowledgement} />}
      </ReviewHeadWrapper>

      {!status.pending && (
        <>
          <Recommendation status={recommendation}>
            recommendation: <span>{recommendation}</span>
            <div>{showReviewerRevision(askedToSeeRevision)}</div>
          </Recommendation>
          <Editor readOnly value={content} />
        </>
      )}

      {/* <Button onClick={pingReviewer} primary>
        Request reviewer&#39;s attention
      </Button> */}

      {isEditor && (
        <Toggle initial={false}>
          {({ on, toggle }) => (
            <>
              <DiscreetButton onClick={toggle}>
                Chat with {reviewer.displayName}
              </DiscreetButton>

              <ChatModal
                headerText={`Chat with reviewer: ${reviewer.displayName}`}
                isOpen={on}
                messages={chatThread.messages}
                onRequestClose={toggle}
                sendMessage={sendChatMessage}
              />
            </>
          )}
        </Toggle>
      )}
    </ReviewWrapper>
  )
}

const Reviews = props => {
  const {
    isEditor,
    // manuscriptId,
    // requestReviewerAttention,
    reviews,
    sendReviewerChatMessage,
  } = props
  // const requestAttention = data =>
  //   requestReviewerAttention({
  //     variables: {
  //       input: {
  //         ...data,
  //         manuscriptId,
  //       },
  //     },
  //   })

  const getRibbonMessage = status => {
    if (status === 'success') return 'Notification successfully sent'
    if (status === 'error') return 'Something went wrong'
    return null
  }

  return (
    <State initial={{ notify: null }}>
      {({ state, setState }) => (
        // const onNotificationSent = successStatus => {
        //   setState({ notify: successStatus })
        //   setTimeout(() => setState({ notify: null }), 4000)
        // }

        <div>
          <PanelSectionHeader>Reviewer feedback</PanelSectionHeader>

          {(!reviews || reviews.length === 0) && (
            <Message>
              No reviewers have accepted their invitation for this article yet
            </Message>
          )}

          {state.notify && (
            <Ribbon
              message={getRibbonMessage(state.notify)}
              status={state.notify}
            />
          )}

          {reviews && reviews.length > 0 && (
            <List
              component={Review}
              isEditor={isEditor}
              items={reviews}
              // onNotificationSent={onNotificationSent}
              // requestReviewerAttention={requestAttention}
              sendReviewerChatMessage={sendReviewerChatMessage}
            />
          )}
        </div>
      )}
    </State>
  )
}

export default Reviews
