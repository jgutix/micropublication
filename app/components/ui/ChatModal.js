/* eslint-disable react/prop-types */
import React from 'react'

import Discuss from './Discuss'
import Modal from '../../../ui/src/modals/Modal'
import ModalHeader from '../../../ui/src/modals/ModalHeader'

const ChatModal = props => {
  const { headerText, messages, sendMessage, ...rest } = props

  const Header = <ModalHeader text={headerText || 'Chat'} />

  return (
    <Modal headerComponent={Header} size="large" {...rest}>
      <Discuss data={messages} sendChat={sendMessage} />
    </Modal>
  )
}

export default ChatModal
