/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import DashboardItemDate from './DashboardItemDate'
import StatusItem from './StatusItem'
import SectionItem from './SectionItem'

const StatusRowWrapper = styled.div`
  display: flex;
  /* margin-bottom: calc(${th('gridUnit')}); */
`

const AuthorWrapper = styled.div`
  font-size: ${th('fontSizeBaseSmall')};
  margin-left: ${th('gridUnit')};
`

const Label = styled.span`
  color: ${th('colorBorder')};
  margin-right: calc(${th('gridUnit')} / 2);
  text-transform: capitalize;

  &:before {
    content: '\\2014';
    padding: 0 ${th('gridUnit')};
  }

  &:after {
    content: ':';
  }
`

const Author = props => {
  const { author } = props

  return (
    author && (
      <AuthorWrapper>
        <Label>submitting author</Label>
        {author}
      </AuthorWrapper>
    )
  )
}

const StatusRow = props => {
  const { author, displayStatus, updated } = props

  const authorValue = author ? `${author.firstName} ${author.lastName}` : ''

  return (
    <StatusRowWrapper>
      <StatusItem label={displayStatus} />
      {updated && <DashboardItemDate label="Last updated" value={updated} />}
      {author && <Author author={authorValue} />}
    </StatusRowWrapper>
  )
}

const SectionItemWithStatus = props => {
  const { actionsComponent, author, displayStatus, title, updated } = props

  return (
    <>
      <StatusRow
        author={author}
        displayStatus={displayStatus}
        updated={updated}
      />
      <SectionItem rightComponent={actionsComponent} title={title} />
    </>
  )
}

export default SectionItemWithStatus
