import React from 'react'

import Accordion from './Accordion'
import Discuss from './Discuss'

const DiscussSection = props => (
  <Accordion label="Chat / Suggest Reviewer">
    <Discuss {...props} />
  </Accordion>
)

export default DiscussSection
