/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'

import { Action, ActionGroup } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import { isEditableByAuthor } from '../../helpers/status'
import SectionItemWithStatus from './SectionItemWithStatus'

const Wrapper = styled.div`
  border-bottom: ${th('borderWidth')} ${th('borderStyle')}
    ${th('colorFurniture')};
  /* margin-bottom: ${th('gridUnit')}; */
  padding: 4px 0;
`

const ActionsWrapper = styled.div`
  flex-shrink: 0;
`

const Actions = props => {
  const { articleId, deleteArticle, status } = props
  const isEditable = isEditableByAuthor(status)

  return (
    <ActionsWrapper>
      <ActionGroup>
        <Action
          data-test-id={`author-action-edit-${articleId}`}
          to={`/article/${articleId}`}
        >
          {isEditable ? 'Edit' : 'View Article'}
        </Action>

        {isEditable && (
          <Action
            onClick={() => deleteArticle({ variables: { id: articleId } })}
          >
            Delete
          </Action>
        )}
      </ActionGroup>
    </ActionsWrapper>
  )
}

const AuthorSectionItem = props => {
  const { deleteArticle, id, status } = props

  const ActionsComponent = (
    <Actions articleId={id} deleteArticle={deleteArticle} status={status} />
  )

  return (
    <Wrapper data-test-id="dashboard-section-item-author">
      <SectionItemWithStatus actionsComponent={ActionsComponent} {...props} />
    </Wrapper>
  )
}

export default AuthorSectionItem
