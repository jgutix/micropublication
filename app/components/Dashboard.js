/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { State } from 'react-powerplug'

import { Action as UIAction } from '@pubsweet/ui'
import {
  ArticlePreviewModal,
  AuthorSectionItem,
  EditorSectionItem,
  ReviewerSectionItem,
  Section,
} from './ui'

import ComposedDashboard from './compose/Dashboard'
import Loading from './Loading'
import { DASHBOARD_MANUSCRIPTS } from './compose/pieces/dashboardManuscripts'
import { CURRENT_USER } from './Private'
import Select from './ui/Select'

const SubmitButton = props => {
  const { client, createManuscript, currentUser, history } = props

  const onClick = () => {
    createManuscript().then(res => {
      const manuscriptId = res.data.createManuscript

      /* 
        TO DO -- This needs to go. See comment in mutation.
      */
      client
        .query({
          fetchPolicy: 'network-only',
          query: CURRENT_USER,
        })
        .then(() => {
          client.query({
            fetchPolicy: 'network-only',
            query: DASHBOARD_MANUSCRIPTS,
            variables: { reviewerId: currentUser.id },
          })

          history.push(`/article/${manuscriptId}`)
        })
    })
  }

  return (
    <Action data-test-id="new-submission-button" onClick={onClick}>
      New Submission
    </Action>
  )
}

const Action = styled(UIAction)`
  line-height: unset;
`

const DashboardWrapper = styled.div`
  margin: 0 auto;
  max-width: 1024px;
`

const Dashboard = props => {
  const {
    allEditors,
    allScienceOfficers,
    authorArticles,
    client,
    createManuscript,
    currentUser,
    deleteArticle,
    editorArticles,
    handleInvitation,
    history,
    loading,
    reviewerArticles,
    scienceOfficerArticles,
    sectionEditorArticles,
    updateAssignedEditor,
    updateAssignedScienceOfficer,
  } = props

  if (loading) return <Loading />

  const isAdmin = currentUser.admin
  const isEditor = currentUser.auth.isGlobalEditor
  const isScienceOfficer = currentUser.auth.isGlobalScienceOfficer
  const isSectionEditor = currentUser.auth.isGlobalSectionEditor

  const options = [
    {
      label: 'Submitted- Needs Triage',
      value: 'submitted- needs triage',
    },
    {
      label: 'Datatype Selected',
      value: 'datatype selected',
    },
    {
      label: 'Submitted',
      value: 'submitted',
    },
    {
      label: 'Approved By Science Officer',
      value: 'approved by science officer',
    },
    {
      label: 'Not Approved By Science Officer',
      value: 'not approved by science officer',
    },
    {
      label: 'Reviewer Invited',
      value: 'reviewer invited',
    },
    {
      label: 'Reviewer Accepted',
      value: 'reviewer accepted',
    },
    {
      label: 'Review Submitted',
      value: 'review submitted',
    },
    {
      label: 'Under Revision',
      value: 'under revision',
    },
    {
      label: 'Accepted',
      value: 'accepted',
    },
    {
      label: 'Rejected',
      value: 'rejected',
    },
  ]

  const headerActions = [
    <SubmitButton
      client={client}
      createManuscript={createManuscript}
      currentUser={currentUser}
      history={history}
      key="createManuscript"
    />,
  ]

  const storedStatuses = localStorage.getItem('selectedStatuses')
    ? JSON.parse(localStorage.getItem('selectedStatuses'))
    : []

  const initialArticles = () => {
    if (storedStatuses.length > 0) {
      return editorArticles.filter(article =>
        storedStatuses.map(s => s.value).includes(article.displayStatus),
      )
    }
    return editorArticles
  }

  return (
    <State
      initial={{
        filteredArticles: initialArticles(),
        previewData: null,
        savedStatuses: storedStatuses,
        showModal: false,
      }}
    >
      {({ state, setState }) => {
        const { previewData, showModal, savedStatuses } = state

        const openModal = article =>
          setState({
            previewData: article,
            showModal: true,
          })

        const closeModal = () =>
          setState({
            previewData: null,
            showModal: false,
          })

        const openReviewerPreviewModal = articleId => {
          const article = reviewerArticles.find(a => a.id === articleId)
          openModal(article)
        }

        const filterArticles = selectedStatuses => {
          localStorage.setItem(
            'selectedStatuses',
            JSON.stringify(selectedStatuses),
          )
          setState({ savedStatuses: selectedStatuses })
          if (selectedStatuses.length > 0) {
            const filteredArticles = editorArticles.filter(article =>
              selectedStatuses
                .map(s => s.value)
                .includes(article.displayStatus),
            )
            setState({ filteredArticles })
          } else {
            setState({ filteredArticles: editorArticles })
          }
        }

        const editorActions = [
          <Select
            closeMenuOnSelect={false}
            isMulti
            onChange={filterArticles}
            options={options}
            placeholder="Filter By Status"
            value={savedStatuses}
          />,
        ]

        return (
          <>
            <DashboardWrapper>
              <Section
                actions={headerActions}
                deleteArticle={deleteArticle}
                itemComponent={AuthorSectionItem}
                items={authorArticles}
                label="My Articles"
              />

              <Section
                handleInvitation={handleInvitation}
                itemComponent={ReviewerSectionItem}
                items={reviewerArticles}
                label="Reviews"
                openReviewerPreviewModal={openReviewerPreviewModal}
              />

              {(isEditor || isAdmin) && (
                <Section
                  actions={editorActions}
                  allEditors={allEditors}
                  allScienceOfficers={allScienceOfficers}
                  itemComponent={EditorSectionItem}
                  items={state.filteredArticles}
                  label="Editor Section"
                  updateAssignedEditor={updateAssignedEditor}
                  updateAssignedScienceOfficer={updateAssignedScienceOfficer}
                  variant="editor"
                />
              )}

              {isSectionEditor && !isEditor && (
                <Section
                  actions={editorActions}
                  allEditors={allEditors}
                  allScienceOfficers={allScienceOfficers}
                  itemComponent={EditorSectionItem}
                  items={sectionEditorArticles}
                  label="Editor Section"
                  updateAssignedEditor={updateAssignedEditor}
                  updateAssignedScienceOfficer={updateAssignedScienceOfficer}
                  variant="editor"
                />
              )}

              {isScienceOfficer && (
                <Section
                  itemComponent={EditorSectionItem}
                  items={scienceOfficerArticles}
                  label="Science Officer Section"
                />
              )}
            </DashboardWrapper>

            <ArticlePreviewModal
              article={previewData}
              isOpen={showModal}
              onRequestClose={closeModal}
            />
          </>
        )
      }}
    </State>
  )
}

const Composed = props => <ComposedDashboard render={Dashboard} {...props} />
export default Composed
