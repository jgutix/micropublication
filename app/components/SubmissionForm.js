/* eslint-disable react/prop-types */

import React, { useState } from 'react'
import styled from 'styled-components'
import {
  cloneDeep,
  concat,
  debounce,
  // get,
  isEqual,
  isString,
  merge,
  mergeWith,
  omit,
  reduce,
  values as lodashValues,
} from 'lodash'
import * as yup from 'yup'
import { v4 as uuid } from 'uuid'

import { th } from '@pubsweet/ui-toolkit'
import { Button } from '@pubsweet/ui'

import { ArticlePreviewModal, PageHeader as Header } from './ui'
import SubmissionConfirmationModal from '../../ui/src/modals/SubmissionConfirmationModal'
// import SubmissionSuccessModal from '../../ui/src/modals/SubmissionSuccessModal'
import InitialSubmission from './formElements/InitialSubmission'
// import GeneExpressionForm from './formElements/GeneExpressionForm'

import Form from './form/Form'

const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 1024px;
`

const PageHeader = styled(Header)`
  margin-top: 0;
`

const InfoError = styled.div`
  color: ${th('colorError')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  margin-top: ${th('gridUnit')};
`

// const options = {
//   dataType: [
//     {
//       label: 'Gene expression results',
//       value: 'geneExpression',
//     },
//     {
//       label: 'No Datatype',
//       value: 'noDatatype',
//     },
//   ],
// }

// const SubmissionFormOne = props => {
//   const {
//     datatypeSelected,
//     errors,
//     full,
//     handleSubmit,
//     initial,
//     isAuthor,
//     isGlobal,
//     isValid,
//     readOnly,
//     revisionState,
//     submitCount,
//     values,
//   } = props

//   return (
//     <>
//       {(fullSubmissionState || revisionState) &&
//         values.dataType === 'geneExpression' &&
//         isAuthor && <GeneExpressionForm readOnly={readOnly} {...props} />}

//       {(dataTypeSelectionState || !isValid) && (
//         <Button data-test-id="submit-button" primary type="submit">
//           Submit
//         </Button>
//       )}
//     </>
//   )
// }

class AutoSave extends React.Component {
  // state = {
  //   isSaving: false,
  //   lastSaved: null,
  //   saveError: null,
  // }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!isEqual(nextProps.values, this.props.values)) {
      // Make sure updates are not sent when the datatype is changed by the editor
      if (isEqual(nextProps.values.dataType, this.props.values.dataType)) {
        this.save()
      }
    }
  }

  save = debounce(() => {
    const { values } = this.props

    const toOmit = ['dataType', 'decisionLetter', 'status']
    if (!(values.image && values.image.name && values.image.url)) {
      toOmit.push('image')
    }
    // const vals = omit(formValuesToData(values), toOmit)
    const vals = omit(values, toOmit)

    Promise.resolve(this.props.onSave(vals))
      .then(a => {
        // this.setState({
        //   isSaving: false,
        //   lastSaved: new Date(),
        // })
      })
      .catch(e => {
        // this.setState({
        //   isSaving: false,
        //   saveError: e,
        // })
      })
  }, 500)

  render() {
    // return this.props.render(this.state)
    return null
  }
}

const defaultValues = {
  acknowledgements: '',
  authors: [
    {
      affiliations: [''],
      correspondingAuthor: false,
      credit: [''],
      email: '',
      equalContribution: false,
      firstName: '',
      lastName: '',
      orcid: '',
      submittingAuthor: false,
      WBId: '',
    },
  ],
  comments: '',
  disclaimer: false,
  funding: '',
  geneExpression: {
    antibodyUsed: '',
    backboneVector: {
      name: '',
      WBId: '',
    },
    coinjected: '',
    constructComments: '',
    constructionDetails: '',
    detectionMethod: '',
    dnaSequence: [
      {
        id: uuid(),
        name: '',
        WBId: '',
      },
    ],
    expressionPattern: {
      name: '',
      WBId: '',
    },
    fusionType: {
      name: '',
      WBId: '',
    },
    genotype: '',
    injectionConcentration: '',
    inSituDetails: '',
    integratedBy: {
      name: '',
      WBId: '',
    },
    observeExpression: {
      certainly: [
        {
          certainly: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          during: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          subcellularLocalization: {
            id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      not: [
        {
          during: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          not: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      partially: [
        {
          during: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          partially: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
      possibly: [
        {
          during: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          id: uuid(),
          possibly: {
            id: uuid(),
            name: '',
            WBId: '',
          },
          subcellularLocalization: {
            id: uuid(),
            name: '',
            WBId: '',
          },
        },
      ],
    },
    reporter: {
      name: '',
      WBId: '',
    },
    species: {
      name: '',
      WBId: '',
    },
    strain: '',
    transgeneName: '',
    transgeneUsed: [
      {
        id: uuid(),
        name: '',
        WBId: '',
      },
    ],
    utr: {
      name: '',
      WBId: '',
    },
    variation: {
      name: '',
      WBId: '',
    },
  },
  image: {},
  imageCaption: '<p></p>',
  laboratory: {
    name: '',
    WBId: '',
  },
  methods: '<p></p>',
  patternDescription: '<p></p>',
  reagents: '<p></p>',
  references: [{ reference: '<p></p>', pubmedId: '', doi: '' }],
  suggestedReviewer: {
    name: '',
    WBId: '',
  },
  title: '',
}

const buildInitialValues = data => {
  const defaultVals = cloneDeep(defaultValues)
  const incomingVals = cloneDeep(data)

  return mergeWith(defaultVals, incomingVals, (defaultValue, incomingValue) => {
    if (Array.isArray(defaultValue)) {
      return incomingValue === null ? defaultValue : lodashValues(incomingValue)
    }
    return incomingValue === null ? defaultValue : incomingValue
  })
}

const isArrayEmpty = arr =>
  !arr || arr.length === 0 || (arr.length === 1 && arr[0] === '')

const isStringFieldEmpty = str => !str

const stripHTML = html => {
  const tmp = document.createElement('DIV')
  tmp.innerHTML = html
  return tmp.textContent || tmp.innerText || ''
}

// eslint-disable-next-line func-names, prefer-arrow-callback, object-shorthand
const validateWBExists = function(val) {
  if (!val) return true

  const { WBId } = this.parent
  if (!WBId || !WBId.length) return false

  return true
}

const initialValidations = {
  acknowledgements: yup.string(),
  // author: yup.object().shape({
  //   affiliations: yup
  //     .array()
  //     .compact()
  //     .of(yup.string())
  //     .required('Must provide at least one author affiliation'),
  //  credit: yup
  //     .array()
  //     .of(yup.string().required('Must choose credit to assign to the author'))
  //     .required('Must choose credit to assign to the author'),
  //   email: yup
  //     .string()
  //     .required('Email is required')
  //     .email('Invalid email adress')
  //     .nullable(),
  //   name: yup.string().required('Name is required'),
  // .test(
  //   'is author valid',
  //   'Must be a registered WormBase Person',
  //   validateWBExists,
  // ),
  //  WBId: yup.string(),
  // }),
  authors: yup
    .array(
      yup.object().shape({
        affiliations: yup
          .array()
          .compact()
          .of(yup.string())
          .test(
            'author affiliations pass if all fields are empty',
            'Affiliations are required for all authors',
            // eslint-disable-next-line func-names, prefer-arrow-callback
            function(val) {
              const { credit, lastName, email } = this.parent
              if (
                isArrayEmpty(credit) &&
                isStringFieldEmpty(lastName) &&
                isStringFieldEmpty(email)
              )
                return true
              if (isArrayEmpty(val)) return false
              return true
            },
          ),
        credit: yup
          .array()
          .of(yup.string())
          .test(
            'author has credit',
            'Credit is required for all authors',
            // eslint-disable-next-line func-names, prefer-arrow-callback
            function(val) {
              const { affiliations, lastName, email } = this.parent

              if (
                isArrayEmpty(affiliations) &&
                isStringFieldEmpty(lastName) &&
                isStringFieldEmpty(email)
              ) {
                return true
              }

              if (isArrayEmpty(val)) return false

              return true
            },
          )
          .nullable(),
        email: yup
          .string()
          .trim()
          .email('Invalid email format')
          .test(
            'email pass if all fields empty',
            'Email is required for all authors',
            // eslint-disable-next-line func-names, prefer-arrow-callback
            function(val) {
              if (val && val.length > 0) return true

              const { affiliations, credit, lastName } = this.parent
              if (
                isArrayEmpty(affiliations) &&
                isArrayEmpty(credit) &&
                isStringFieldEmpty(lastName)
              ) {
                return true
              }

              return false
            },
          ),
        firstName: yup.string().test(
          'author pass if all fields empty',
          'First name is required for all authors',
          // eslint-disable-next-line func-names, prefer-arrow-callback
          function(val) {
            if (val && val.length > 0) return true

            const { affiliations, credit, email } = this.parent
            if (
              isArrayEmpty(affiliations) &&
              isArrayEmpty(credit) &&
              isStringFieldEmpty(email)
            ) {
              return true
            }

            return false
          },
        ),
        lastName: yup.string().test(
          'author pass if all fields empty',
          'Last name is required for all authors',
          // eslint-disable-next-line func-names, prefer-arrow-callback
          function(val) {
            if (val && val.length > 0) return true

            const { affiliations, credit, email } = this.parent
            if (
              isArrayEmpty(affiliations) &&
              isArrayEmpty(credit) &&
              isStringFieldEmpty(email)
            ) {
              return true
            }

            return false
          },
        ),
        ordid: yup.string(),
        // .test(
        //   'is co-author valid',
        //   'Must be a registered WormBase Person',
        //   validateWBExists,
        // )
      }),
    )
    .test(
      'has one corresponding author',
      'At least one author must be set as corresponding',
      val => {
        const corresponding = []
        val.forEach(author => {
          if (author.correspondingAuthor) {
            corresponding.push(author)
          }
        })
        return corresponding.length > 0
      },
    )
    .test(
      'has one submitting author',
      'One author must be set as submitting',
      val => {
        const submitting = []
        val.forEach(author => {
          if (author.submittingAuthor) {
            submitting.push(author)
          }
        })
        return submitting.length === 1
      },
    ),
  comments: yup.string(),
  disclaimer: yup.boolean().test('disclaimer', 'Required', val => val),
  funding: yup.string().required('Funding is required'),
  image: yup.object().shape({
    url: yup.string().required('Image is required'),
  }),
  imageCaption: yup
    .string()
    .test(
      'image-caption-not-empty',
      'Image caption is required',
      val => stripHTML(val).length > 0,
    ),
  laboratory: yup.object().shape({
    name: yup.string(),
    // .test(
    //   'is-lab-valid',
    //   'Must a registered WormBase Laboratory',
    //   validateWBExists,
    // )
    WBId: yup.string(),
  }),
  methods: yup.string(),
  // .test(
  // 'methods-not-empty',
  //  'Methods & Reagents is required',
  //  val => stripHTML(val).length > 0,
  // ),
  patternDescription: yup
    .string()
    .test(
      'pattern-description-not-empty',
      'Text is required',
      val => stripHTML(val).length > 0,
    ),
  reagents: yup.string(),
  references: yup.array(
    yup
      .object()
      .shape({
        doi: yup.string(),
        pubmedId: yup.string(), // 1-8 digit number
        reference: yup.string(),
      })
      .test(
        'reference and either pubmedid or doi',
        'Reference text required',
        val => {
          const { doi, pubmedId, reference } = val
          if (
            isStringFieldEmpty(stripHTML(reference)) &&
            isStringFieldEmpty(doi) &&
            isStringFieldEmpty(pubmedId)
          ) {
            return true
          }
          if (isStringFieldEmpty(stripHTML(reference))) {
            return false
          }
          return true
        },
      ),
  ),
  // .test(
  //  'references-not-empty',
  //  'References are required',
  //  val => stripHTML(val).length > 0,
  // ),
  suggestedReviewer: yup.object().shape({
    name: yup.string(),
    // .test(
    //   'is suggested reviewer valid',
    //   'Must be a registered WormBase Person',
    //   validateWBExists,
    // )
  }),
  title: yup
    .string()
    .test(
      'title-not-empty',
      'Title is required',
      val => stripHTML(val).length > 0,
    ),
}

const geneExpression = {
  geneExpression: yup.object().shape({
    antibodyUsed: yup.string().when(['detectionMethod'], {
      is: val => val === 'antibody',
      then: yup.string().required('Antibody is required'),
    }),
    backboneVector: yup
      .object()
      .shape({
        name: yup.string(),
        WBId: yup.string(),
      })
      .when('detectionMethod', {
        is: val => val === 'newTransgene',
        then: yup.object().shape({
          name: yup
            .string()
            .test(
              'is backbone vector valid',
              'Must be a valid WormBase vector',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
    coinjected: yup.string(),
    constructComments: yup.string(),
    constructionDetails: yup.string().when(['detectionMethod'], {
      is: val => val === 'newTransgene',
      then: yup.string().required('Construction details are required'),
    }),
    detectionMethod: yup.string().required('Detection method is required'),
    dnaSequence: yup
      .array()
      .of(
        yup.object().shape({
          name: yup.string(),
        }),
      )
      .when(['detectionMethod'], {
        is: val => val === 'newTransgene',
        then: yup
          .array()
          .of(
            yup.object().shape({
              name: yup.string(),
            }),
          )
          .min(1, 'Provide at least one DNA sequence')
          .max(10)
          .compact(val => val.name === ''),
      }),
    expressionPattern: yup.object().shape({
      name: yup
        .string()
        .required('Expression pattern is required')
        .test(
          'is expression pattern valid',
          'Must be a valid WormBase expression pattern',
          validateWBExists,
        ),
      WBId: yup.string(),
    }),
    fusionType: yup
      .object()
      .shape({
        name: yup.string(),
        WBId: yup.string(),
      })
      .when(['detectionMethod'], {
        is: val => val === 'newTransgene',
        then: yup.object().shape({
          name: yup
            .string()
            .required('Fusion type is required')
            .test(
              'is fusion type valid',
              'Must be a valid WormBase fusion type',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
    genotype: yup.string().when(['detectionMethod'], {
      is: val => val === 'newTransgene',
      then: yup.string().required('Genotype is required'),
    }),
    injectionConcentration: yup.string(),
    inSituDetails: yup.string().when(['detectionMethod'], {
      is: val => val === 'inSituHybridization',
      then: yup.string().required('In Situ Details are required'),
    }),
    integratedBy: yup
      .object()
      .shape({
        name: yup.string(),
        WBId: yup.string(),
      })
      .when(['detectionMethod'], {
        is: val => val === 'newTransgene',
        then: yup.object().shape({
          name: yup
            .string()
            .required('Integration type is required')
            .test(
              'is integration type valid',
              'Must be a valid WormBase integration type',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
    observeExpression: yup
      .object()
      .shape({
        certainly: yup.array().of(
          yup.object().shape({
            certainly: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is certainly valid',
                  'Expression fields must be valid WormBase body locations',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            during: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is certainly during valid',
                  'During fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            id: yup.string().nullable(),
            subcellularLocalization: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is certainly subcell valid',
                  'Subcellular localization fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
          }),
        ),
        not: yup.array().of(
          yup.object().shape({
            during: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is not during valid',
                  'During fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            id: yup.string().nullable(),
            not: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is not valid',
                  'Expression fields must be valid WormBase body locations',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            subcellularLocalization: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is not subcell valid',
                  'Subcellular localization fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
          }),
        ),
        partially: yup.array().of(
          yup.object().shape({
            during: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is partially during valid',
                  'During fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            id: yup.string().nullable(),
            partially: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is partially valid',
                  'Expression fields must be valid WormBase body locations',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            subcellularLocalization: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is partially subcell valid',
                  'Subcellular localization fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
          }),
        ),
        possibly: yup.array().of(
          yup.object().shape({
            during: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is possibly during valid',
                  'During fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            id: yup.string().nullable(),
            possibly: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is possibly valid',
                  'Expression fields must be valid WormBase body locations',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
            subcellularLocalization: yup.object().shape({
              name: yup
                .string()
                .test(
                  'is possibly subcell valid',
                  'Subcellular localization fields must be valid WormBase life stages',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
          }),
        ),
      })
      .test('observe expression test', 'Fill in at least one field', val => {
        const flatFirstLevel = reduce(lodashValues(val), (result, item) =>
          concat(result, item),
        )

        let flatValues = []

        flatFirstLevel.forEach(obj => {
          flatValues = concat(
            flatValues,
            lodashValues(obj).filter(item => !isString(item)),
          )
        })

        return flatValues.find(item => {
          if (item === null) return false
          return (
            (item.value && item.value.length > 0) ||
            (item.name && item.name.length > 0)
          )
        })
      }),
    reporter: yup
      .object()
      .shape({
        name: yup.string(),
        WBId: yup.string(),
      })
      .when(['detectionMethod'], {
        is: val => val === 'newTransgene',
        then: yup.object().shape({
          name: yup
            .string()
            .required('Reporter is required')
            .test(
              'is reporter valid',
              'Must be a valid WormBase protein',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
    species: yup.object().shape({
      name: yup
        .string()
        .required('Species is required')
        .test(
          'is species valid',
          'Must be a valid WormBase species',
          validateWBExists,
        ),
      WBId: yup.string(),
    }),
    strain: yup.string(),
    transgeneName: yup.string().when(['detectionMethod'], {
      is: val => val === 'newTransgene',
      then: yup.string().required('Transgene name is required'),
    }),
    transgeneUsed: yup
      .array()
      .of(
        yup.object().shape({
          name: yup.string(),
          WBId: yup.string(),
        }),
      )
      .when(['detectionMethod'], {
        is: val => val === 'existingTransgene',
        then: yup
          .array()
          .of(
            yup.object().shape({
              name: yup
                .string()
                .test(
                  'is transgene valid',
                  'Must be a valid WormBase transgene',
                  validateWBExists,
                ),
              WBId: yup.string(),
            }),
          )
          .min(1, 'Provide at least one transgene')
          .max(10)
          .compact(val => val.name === ''),
      }),
    utr: yup
      .object()
      .shape({
        name: yup.string(),
        WBId: yup.string(),
      })
      .when('detectionMethod', {
        is: val => val === 'newTransgene',
        then: yup.object().shape({
          name: yup
            .string()
            .test(
              'is utr valid',
              'Must be a valid WormBase gene',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
    variation: yup
      .object()
      .shape({
        name: yup
          .string()
          .test(
            'is variation valid',
            'Must be a valid WormBase variation',
            validateWBExists,
          ),
        WBId: yup.string(),
      })
      .when('detectionMethod', {
        is: val => val === 'genomeEditing',
        then: yup.object().shape({
          name: yup
            .string()
            .required('Variation is required')
            .test(
              'is variation valid',
              'Must be a valid WormBase variation',
              validateWBExists,
            ),
          WBId: yup.string(),
        }),
      }),
  }),
}

const makeSchema = vals => {
  const schema = cloneDeep(initialValidations)

  if (vals.dataType === 'geneExpression') {
    merge(schema, geneExpression)
  }

  return yup.object().shape(schema)
}

const SubmissionForm = props => {
  const { data, saveForm, submissionType, submitManuscript, upload } = props

  const [showPreviewModal, setShowPreviewModal] = useState(false)
  const [showConfirmationModal, setShowConfirmationModal] = useState(false)
  // const [showSuccessModal, setShowSuccessModal] = useState(false)

  //   const initialSubmissionState = !initial
  //   const dataTypeSelectionState = initial && !datatypeSelected
  //   const fullSubmissionState = initial && datatypeSelected && !full

  // const { datatypeSelected, initial } = otherProps
  // const dataTypeSelectionState = initial && !datatypeSelected

  const handleSubmit = (formValues, formikBag) => {
    submitManuscript(formValues).then(() => {
      setShowConfirmationModal(false)
      // setShowSuccessModal(true)
    })
  }

  const initialValues = buildInitialValues(data)
  const validations = makeSchema(initialValues)
  const isInitialValid = validations.isValidSync(initialValues)

  return (
    <Wrapper>
      <Form
        // enableReinitialize // breaks autosave
        initialValues={initialValues}
        isInitialValid={isInitialValid}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            handleBlur,
            handleChange,
            isValid,
            setFieldTouched,
            setFieldValue,
            touched,
            submitCount,
            submitForm,
            values,
          } = formProps

          // If not valid, this will increment submitcount and show InfoError
          const handleClickSubmit = () =>
            isValid ? setShowConfirmationModal(true) : submitForm()

          return (
            <>
              <PageHeader>Submission form</PageHeader>

              <InitialSubmission
                errors={errors}
                handleBlur={handleBlur}
                handleChange={handleChange}
                setFieldTouched={setFieldTouched}
                setFieldValue={setFieldValue}
                touched={touched}
                upload={upload}
                values={values}
              />

              <Button onClick={() => setShowPreviewModal(true)}>Preview</Button>

              <Button onClick={handleClickSubmit} primary>
                Submit
              </Button>

              {submitCount > 0 && !isValid && (
                <InfoError>
                  Please review submission and fill out all required fields.
                </InfoError>
              )}

              <ArticlePreviewModal
                isOpen={showPreviewModal}
                onRequestClose={() => setShowPreviewModal(false)}
                values={values}
              />

              <SubmissionConfirmationModal
                // isFullSubmission={fullSubmissionState && !revisionState}
                isInitialSubmission={submissionType === 'initial'}
                isOpen={showConfirmationModal}
                isRevisionSubmission={submissionType === 'revision'}
                onConfirm={submitForm}
                onRequestClose={() => setShowConfirmationModal(false)}
              />

              {/* <SubmissionSuccessModal
              isOpen={showSuccessModal}
              onRequestClose={() => setShowSuccessModal(false)}
            /> */}

              <AutoSave onSave={saveForm} values={values} />
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

export default SubmissionForm
