/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { Toggle } from 'react-powerplug'
import { debounce, first, isEqual, last, omit } from 'lodash'

import { DateParser, Icon } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'

import ComposedSubmit from './compose/Submit'
import ReviewerPanel from './compose/ReviewerPanel'
import EditorPanel from './EditorPanel'
import SubmitForm from './form/SubmissionForm'
import Loading from './Loading'
import SubmissionForm from './SubmissionForm'
import { ArticlePreview, PageHeader, Ribbon, Tabs } from './ui'
import SubmissionSuccessModal from '../../ui/src/modals/SubmissionSuccessModal'
import { formValuesToData } from './formElements/helpers'
import TabContext from '../tabContext'

const PageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`

const StyledPageHeader = styled(PageHeader)`
  margin-bottom: 0;
  margin-top: calc(${th('gridUnit')} * 2);
`

const TopRow = styled.div`
  display: flex;
  margin-bottom: ${th('gridUnit')};
`

const TopRowMessage = styled(Ribbon)`
  flex-grow: 1;
  margin-bottom: 0;
  margin-right: calc(${th('gridUnit')} * 2);
`

const LockWrapper = styled.div`
  align-items: center;
  border-radius: 2px;
  cursor: pointer;
  display: flex;
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  padding: 0 calc(${th('gridUnit')} / 2) 0 ${th('gridUnit')};
  text-transform: uppercase;
  transition: background ${th('transitionDuration')}
    ${th('transitionTimingFunction')};
  user-select: none;

  :hover {
    background: ${th('colorBackgroundHue')};
  }

  > :first-child {
    align-items: center;
    color: ${th('colorPrimary')};
    display: inline-flex;
    margin-top: 4px;
  }
`

const SplitScreen = styled.div`
  display: flex;
  flex-grow: 1;
  /* Minus the height of the lock tabs row */
  height: calc(100% - calc(${th('gridUnit')} * 4));

  > div {
    border: ${th('borderWidth')} ${th('borderStyle')}
      ${th('colorBackgroundHue')};
    border-radius: ${th('borderRadius')};
    height: 100%;
    overflow-y: auto;
    padding: 0 ${th('gridUnit')};
    width: 50%;
  }

  > div:last-child {
    margin-left: ${th('gridUnit')};
  }
`

const Lock = ({ locked, onClick }) => {
  const action = `${locked ? 'lock' : 'unlock'}`
  const actionText = `${locked ? 'unlock' : 'lock'}`
  const message = `Click to ${actionText} tabs`

  return (
    <LockWrapper onClick={onClick}>
      <span>{message}</span>
      <Icon size={2}>{action}</Icon>
    </LockWrapper>
  )
}

class AutoSave extends React.Component {
  // state = {
  //   isSaving: false,
  //   lastSaved: null,
  //   saveError: null,
  // }

  componentWillReceiveProps(nextProps, nextContext) {
    if (!isEqual(nextProps.values, this.props.values)) {
      // Make sure updates are not sent when the datatype is changed by the editor
      if (isEqual(nextProps.values.dataType, this.props.values.dataType)) {
        this.save()
      }
    }
  }

  save = debounce(() => {
    const { values } = this.props

    const toOmit = ['dataType', 'decisionLetter', 'status']
    if (!(values.image && values.image.name && values.image.url)) {
      toOmit.push('image')
    }
    const vals = omit(formValuesToData(values), toOmit)

    Promise.resolve(this.props.onSave(vals))
      .then(a => {
        // this.setState({
        //   isSaving: false,
        //   lastSaved: new Date(),
        // })
      })
      .catch(e => {
        // this.setState({
        //   isSaving: false,
        //   saveError: e,
        // })
      })
  }, 500)

  render() {
    // return this.props.render(this.state)
    return null
  }
}

const FormElement = props => {
  const {
    article,
    datatypeSelected,
    full,
    initial,
    isAuthor,
    isGlobal,
    readOnly,
    revisionState,
    showModal,
    saveForm,
    setDataType,
    submitManuscript,
    upload,
  } = props

  return (
    <SubmitForm
      article={article}
      datatypeSelected={datatypeSelected}
      full={full}
      initial={initial}
      isAuthor={isAuthor}
      isGlobal={isGlobal}
      readOnly={readOnly}
      revisionState={revisionState}
      setDataType={setDataType}
      showModal={showModal}
      submitManuscript={submitManuscript}
      upload={upload}
    >
      {formProps => (
        <div>
          <h1>Submit your article</h1>
          <SubmissionForm article={article} upload={upload} {...formProps} />
          <AutoSave onSave={saveForm} values={formProps.values} />
        </div>
      )}
    </SubmitForm>
  )
}

const Form = props => {
  const {
    article,
    dataTypeSelected,
    full,
    initiallySubmitted,
    isAuthor,
    isGlobal,
    saveForm,
    setDataType,
    showModal,
    submitManuscript,
    underRevision,
    upload,
  } = props

  const version = article.versions.find(a => a.active)
  article.version = version

  const readOnly = (initiallySubmitted && !dataTypeSelected) || full
  const revisionState = underRevision

  const formValues = omit(version, ['active', 'decision'])

  return (
    <FormElement
      article={formValues}
      datatypeSelected={dataTypeSelected}
      full={full}
      initial={initiallySubmitted}
      isAuthor={isAuthor}
      isGlobal={isGlobal}
      readOnly={readOnly}
      revisionState={revisionState}
      saveForm={saveForm}
      setDataType={setDataType}
      showModal={showModal}
      submitManuscript={submitManuscript}
      upload={upload}
    />
  )
}

const Submit = props => {
  const {
    article,
    authorChatMessages,
    exportManuscript,
    isAcceptedReviewerForManuscript,
    isAcceptedReviewerForVersion,
    isAuthor,
    isGlobal,
    isReviewer,
    loading,
    saveForm,
    sendAuthorChatMessage,
    setDataType,
    submit,
    upload,
  } = props

  if (loading) return <Loading />

  const { versions } = article

  const version = last(versions)
  version.dataType = article.dataType

  const firstVersion = first(versions)

  const initiallySubmitted = article.isInitiallySubmitted
  const full = version.submitted

  let underRevision
  if (versions.length > 1) {
    const previousVersion = versions[versions.length - 2]
    underRevision = previousVersion.decision === 'revise' && !full
  }

  const dataTypeSelected = article.isDataTypeSelected

  const editableByAuthor =
    !initiallySubmitted || (dataTypeSelected && !full) || underRevision

  /*
    if not full:
    !iseditable and isglobal, show form
    editor will select datatype -> show form
    
    !iseditable and !isglobal
    author and you cannot edit -> show preview
    
    iseditable and isglobal
    initial -> cannot see
    post-datatype -> show preview
    
    iseditable and !isglobal
    author and you can edit -> show form
  */

  return (
    <TabContext.Consumer>
      {tabContextValue => (
        <Toggle initial={false}>
          {({ on, toggle }) => {
            const form = (
              <Form
                article={article}
                dataTypeSelected={dataTypeSelected}
                full={full}
                initiallySubmitted={initiallySubmitted}
                isAuthor={isAuthor}
                isGlobal={isGlobal}
                saveForm={saveForm}
                setDataType={setDataType}
                showModal={toggle}
                submitManuscript={submit}
                underRevision={underRevision}
                upload={upload}
              />
            )

            const preview = (
              <ArticlePreview
                article={version}
                articleId={article.id}
                authorChatMessages={authorChatMessages}
                exportManuscript={exportManuscript}
                sendAuthorChatMessage={sendAuthorChatMessage}
              />
            )

            const makePreviewSections = () => {
              let previewSectionVersions = []

              if (isGlobal) {
                previewSectionVersions = versions.filter(v => v.submitted)
              } else if (isAcceptedReviewerForManuscript) {
                previewSectionVersions = versions.filter(v =>
                  isAcceptedReviewerForVersion.includes(v.id),
                )
              }

              const sections = previewSectionVersions.map((v, index) => ({
                content: (
                  <ArticlePreview
                    article={v}
                    articleId={article.id}
                    exportManuscript={exportManuscript}
                    previousVersion={
                      index > 0 ? previewSectionVersions[index - 1] : null
                    }
                    showHeader={false}
                  />
                ),
                key: v.id,
                label: (
                  <DateParser
                    dateFormat="MM.DD.YY HH:mm"
                    timestamp={new Date(Number(v.created))}
                  >
                    {timestamp => (
                      <span>
                        {firstVersion.id === v.id
                          ? `Original: ${timestamp}`
                          : `Revision ${index}: ${timestamp}`}
                      </span>
                    )}
                  </DateParser>
                ),
              }))

              return sections
            }

            const previewSections = makePreviewSections()

            const {
              activeTab,
              locked,
              toggleLock,
              updateActiveTab,
            } = tabContextValue

            const editorView = (
              <PageWrapper>
                <TopRow>
                  <TopRowMessage
                    message={
                      locked
                        ? ''
                        : `Version tabs between the two panes are not in sync. 
                           Make sure you are looking at the correct version of 
                           the article.`
                    }
                    status="warning"
                  />
                  <Lock locked={locked} onClick={toggleLock} />
                </TopRow>
                <SplitScreen>
                  <div>
                    <StyledPageHeader>Article Preview</StyledPageHeader>
                    <Tabs
                      activeKey={
                        activeTab ||
                        (last(previewSections) && last(previewSections).key)
                      }
                      alwaysUseActiveKeyFromProps={locked}
                      onTabClick={updateActiveTab}
                      sections={previewSections}
                    />
                  </div>
                  <div>
                    {isGlobal && <EditorPanel />}
                    {isAcceptedReviewerForManuscript && <ReviewerPanel />}
                  </div>
                </SplitScreen>
              </PageWrapper>
            )

            let display = null

            if (!full) {
              if (editableByAuthor) {
                if (underRevision && ((isGlobal && !isAuthor) || isReviewer)) {
                  display = editorView
                } else {
                  display = isGlobal && !isAuthor ? preview : form
                }
              } else {
                display = isGlobal && !isAuthor ? form : preview
              }
            }

            if (full && !underRevision) {
              if ((isGlobal || isAcceptedReviewerForManuscript) && !isAuthor) {
                display = editorView
              } else {
                display = preview
              }
            }

            return (
              <React.Fragment>
                {display}
                <SubmissionSuccessModal isOpen={on} onRequestClose={toggle} />
              </React.Fragment>
            )
          }}
        </Toggle>
      )}
    </TabContext.Consumer>
  )
}

const Composed = () => <ComposedSubmit render={Submit} />
export default Composed
