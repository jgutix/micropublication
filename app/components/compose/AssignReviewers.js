/* eslint-disable react/prop-types */

import React from 'react'
import { adopt } from 'react-adopt'
import { withRouter } from 'react-router-dom'
import { get, last } from 'lodash'

import {
  // queries
  getArticleReviewers,
  getGlobalTeams,
  getUsers,

  // mutations
  addExternalReviewer,
  inviteReviewer,
  updateReviewerPool,
} from './pieces'

import { getRegularUsers } from '../../helpers/teams'

/* eslint-disable sort-keys */
const mapper = {
  // queries
  getArticleReviewers: props => getArticleReviewers(props),
  getGlobalTeams,
  getUsers,

  // mutations
  addExternalReviewer,
  inviteReviewer,
  updateReviewerPool,
}
/* eslint-enable sort-keys */

// eslint-disable-next-line arrow-body-style
const mapProps = args => {
  const data = get(args.getArticleReviewers, 'data.manuscript')

  let reviewersTeam, suggested, version

  if (data) {
    version = last(data.versions.filter(v => v.submitted))
    reviewersTeam = version.teams.find(t => t.role === 'reviewer')
    suggested = version.suggestedReviewer
  }

  const addExternalReviewerFn = input =>
    args.addExternalReviewer.addExternalReviewer({
      variables: {
        input,
        manuscriptVersionId: version.id,
      },
    })

  const inviteReviewerFn = reviewerId =>
    args.inviteReviewer.inviteReviewer({
      variables: {
        input: { reviewerId },
        manuscriptVersionId: version.id,
      },
    })

  const updateReviewerPoolFn = input =>
    args.updateReviewerPool.updateReviewerPool({
      variables: {
        input,
        manuscriptVersionId: version.id,
      },
    })

  return {
    addExternalReviewer: addExternalReviewerFn,
    inviteReviewer: inviteReviewerFn,
    loading:
      args.getArticleReviewers.loading ||
      args.getUsers.loading ||
      args.getGlobalTeams.loading,
    reviewersTeam,
    suggested,
    updateReviewerPool: updateReviewerPoolFn,
    users: getRegularUsers(
      get(args.getUsers, 'data.users'),
      get(args.getGlobalTeams, 'data.globalTeams'),
    ),
  }
}

const Composed = adopt(mapper, mapProps)

const ComposedAssignReviewers = props => {
  const { match, render } = props
  const { id: articleId } = match.params

  return (
    <Composed articleId={articleId}>
      {mappedProps => render({ ...mappedProps, articleId })}
    </Composed>
  )
}

export default withRouter(ComposedAssignReviewers)
