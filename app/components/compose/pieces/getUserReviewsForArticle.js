/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from '@apollo/react-components'
import gql from 'graphql-tag'

const GET_USER_REVIEWS_FOR_ARTICLE = gql`
  query GetUserReviewsForArticle($id: ID!) {
    manuscript(id: $id) {
      id
      chatThreads(currentReviewerOnly: true, type: reviewer) {
        id
        messages {
          id
          content
          timestamp
          user {
            displayName
          }
        }
      }
      versions {
        id
        created
        decision
        reviews(currentUserOnly: true) {
          id
          content
          created
          openAcknowledgement
          askedToSeeRevision
          recommendation
          status {
            submitted
          }
        }
      }
    }
  }
`

const GetUserReviewsForArticleQuery = props => {
  const { articleId, render } = props
  const variables = { id: articleId }

  return (
    <Query query={GET_USER_REVIEWS_FOR_ARTICLE} variables={variables}>
      {render}
    </Query>
  )
}

export { GET_USER_REVIEWS_FOR_ARTICLE }
export default GetUserReviewsForArticleQuery
