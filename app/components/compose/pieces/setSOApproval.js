/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE_FOR_EDITOR } from './getArticleForEditor'

const SET_SO_APPROVAL = gql`
  mutation SetSOApproval($manuscriptVersionId: ID!, $approval: Boolean!) {
    setSOApproval(
      manuscriptVersionId: $manuscriptVersionId
      approval: $approval
    )
  }
`

const SetSOApprovalMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_FOR_EDITOR,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SET_SO_APPROVAL} refetchQueries={refetchQueries}>
      {(setSOApproval, setSOApprovalResponse) =>
        render({ setSOApproval, setSOApprovalResponse })
      }
    </Mutation>
  )
}

export default SetSOApprovalMutation
