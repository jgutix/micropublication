/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { withCurrentUser } from '../../../userContext'
import { GET_USER_REVIEWS_FOR_ARTICLE } from './getUserReviewsForArticle'
import { DASHBOARD_MANUSCRIPTS } from './dashboardManuscripts'

const SUBMIT_REVIEW = gql`
  mutation SubmitReview($reviewId: ID!, $input: SubmitReviewInput!) {
    submitReview(reviewId: $reviewId, input: $input)
  }
`

const SubmitReviewMutation = props => {
  const { articleId, currentUser, render } = props

  const refetch = [
    {
      query: GET_USER_REVIEWS_FOR_ARTICLE,
      variables: {
        id: articleId,
      },
    },
    {
      query: DASHBOARD_MANUSCRIPTS,
      variables: {
        reviewerId: currentUser.id,
      },
    },
  ]

  return (
    <Mutation mutation={SUBMIT_REVIEW} refetchQueries={refetch}>
      {(submitReview, submitReviewResponse) =>
        render({ submitReview, submitReviewResponse })
      }
    </Mutation>
  )
}

export default withCurrentUser(SubmitReviewMutation)
