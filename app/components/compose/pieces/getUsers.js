/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from '@apollo/react-components'
import gql from 'graphql-tag'

const GET_USERS = gql`
  query GetUsers {
    users {
      admin
      id
      displayName
      username
    }
  }
`

const GetUsersQuery = props => {
  const { render } = props

  return <Query query={GET_USERS}>{render}</Query>
}

export { GET_USERS }
export default GetUsersQuery
