/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE_FOR_EDITOR } from './getArticleForEditor'

const REINVITE_REVIEWER = gql`
  mutation ReinviteReviewer($input: ReinviteReviewerInput!) {
    reinviteReviewer(input: $input)
  }
`

const ReinviteReviewerMutation = props => {
  const { articleId: id, render } = props

  const refetch = [
    {
      query: GET_ARTICLE_FOR_EDITOR,
      variables: { id },
    },
  ]

  return (
    <Mutation mutation={REINVITE_REVIEWER} refetchQueries={refetch}>
      {(reinviteReviewer, reinviteReviewerResponse) =>
        render({ reinviteReviewer, reinviteReviewerResponse })
      }
    </Mutation>
  )
}

export default ReinviteReviewerMutation
