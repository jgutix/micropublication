/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'
import { GET_ARTICLE_REVIEWERS } from './getArticleReviewers'

const UPDATE_REVIEWER_POOL = gql`
  mutation UpdateReviewerPool(
    $manuscriptVersionId: ID!
    $input: UpdateReviewerPoolInput!
  ) {
    updateReviewerPool(manuscriptVersionId: $manuscriptVersionId, input: $input)
  }
`

const UpdateReviewerPoolMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_REVIEWERS,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={UPDATE_REVIEWER_POOL} refetchQueries={refetchQueries}>
      {(updateReviewerPool, updateReviewerPoolResponse) =>
        render({ updateReviewerPool, updateReviewerPoolResponse })
      }
    </Mutation>
  )
}

export default UpdateReviewerPoolMutation
