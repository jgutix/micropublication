/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE_FOR_EDITOR } from './getArticleForEditor'

const SUBMIT_DECISION = gql`
  mutation SubmitDecision(
    $manuscriptVersionId: ID!
    $input: SubmitDecisionInput!
  ) {
    submitDecision(manuscriptVersionId: $manuscriptVersionId, input: $input)
  }
`

const SubmitDecisionMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_FOR_EDITOR,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SUBMIT_DECISION} refetchQueries={refetchQueries}>
      {(submitDecision, submitDecisionResponse) =>
        render({ submitDecision, submitDecisionResponse })
      }
    </Mutation>
  )
}

export default SubmitDecisionMutation
