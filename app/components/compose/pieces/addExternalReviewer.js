/* eslint-disable react/prop-types */

import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_ARTICLE_REVIEWERS } from './getArticleReviewers'
import { withCurrentUser } from '../../../userContext'

const ADD_EXTERNAL_REVIEWER = gql`
  mutation AddExternalReviewer(
    $manuscriptVersionId: ID!
    $input: AddExternalReviewerInput!
  ) {
    addExternalReviewer(
      manuscriptVersionId: $manuscriptVersionId
      input: $input
    )
  }
`

const AddExternalReviewerMutation = props => {
  const { articleId, render } = props

  const refetchQueries = [
    {
      query: GET_ARTICLE_REVIEWERS,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={ADD_EXTERNAL_REVIEWER} refetchQueries={refetchQueries}>
      {(addExternalReviewer, addExternalReviewerResponse) =>
        render({ addExternalReviewer, addExternalReviewerResponse })
      }
    </Mutation>
  )
}

// TO DO -- withCurrentUser not needed
export default withCurrentUser(AddExternalReviewerMutation)
