/* eslint-disable react/prop-types */
import React from 'react'
import { Mutation } from '@apollo/react-components'
import gql from 'graphql-tag'

import { GET_USER_REVIEWS_FOR_ARTICLE } from './getUserReviewsForArticle'

const SAVE_REVIEW = gql`
  mutation SaveReview($reviewId: ID!, $input: SubmitReviewInput!) {
    saveReview(reviewId: $reviewId, input: $input)
  }
`

const SaveReviewMutation = props => {
  const { articleId, render } = props

  const refetch = [
    {
      query: GET_USER_REVIEWS_FOR_ARTICLE,
      variables: { id: articleId },
    },
  ]

  return (
    <Mutation mutation={SAVE_REVIEW} refetchQueries={refetch}>
      {(saveReview, saveReviewResponse) =>
        render({ saveReview, saveReviewResponse })
      }
    </Mutation>
  )
}

export default SaveReviewMutation
