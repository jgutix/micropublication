/* eslint-disable react/prop-types */

import React from 'react'
import { Query } from '@apollo/react-components'
import gql from 'graphql-tag'

const GET_MANUSCRIPT = gql`
  query manuscript($id: ID!) {
    manuscript(id: $id) {
      id
      chatThreads(type: author) {
        id
        chatType
        messages {
          content
          timestamp
          user {
            displayName
          }
        }
      }
      dataType
      isDataTypeSelected
      isInitiallySubmitted
      versions {
        id
        created
        active
        submitted
        isApprovedByScienceOfficer
        acknowledgements
        authors {
          affiliations
          credit
          email
          firstName
          lastName
          submittingAuthor
          correspondingAuthor
          equalContribution
          WBId
          orcid
        }
        comments
        decision
        disclaimer
        funding
        image {
          name
          url
        }
        imageCaption
        laboratory {
          name
          WBId
        }
        methods
        reagents
        patternDescription
        references {
          reference
          pubmedId
          doi
        }
        suggestedReviewer {
          name
          WBId
        }
        title
        geneExpression {
          antibodyUsed
          backboneVector {
            name
            WBId
          }
          coinjected
          constructComments
          constructionDetails
          detectionMethod
          dnaSequence {
            name
            WBId
          }
          expressionPattern {
            name
            WBId
          }
          fusionType {
            name
            WBId
          }
          genotype
          injectionConcentration
          inSituDetails
          integratedBy {
            name
            WBId
          }
          observeExpression {
            certainly {
              certainly {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            partially {
              partially {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            possibly {
              possibly {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
            not {
              not {
                name
                WBId
              }
              during {
                name
                WBId
              }
              id
              subcellularLocalization {
                name
                WBId
              }
            }
          }
          reporter {
            name
            WBId
          }
          species {
            name
            WBId
          }
          strain
          transgeneName
          transgeneUsed {
            name
            WBId
          }
          utr {
            name
            WBId
          }
          variation {
            name
            WBId
          }
        }
      }
    }
  }
`

const GetArticleQuery = props => {
  const { articleId: id, render } = props

  return (
    <Query query={GET_MANUSCRIPT} variables={{ id }}>
      {render}
    </Query>
  )
}

export { GET_MANUSCRIPT as GET_ARTICLE }
export default GetArticleQuery
