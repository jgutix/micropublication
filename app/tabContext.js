import { createContext } from 'react'

const TabContext = createContext({
  activeTab: null,
  locked: true,
  toggleLock: () => {},
  updateActiveTab: () => {},
})

export default TabContext
