/* eslint-disable sort-keys */

const get = require('lodash/get')
const isEqual = require('lodash/isEqual')
const difference = require('lodash/difference')
const xor = require('lodash/xor')

// const logger = require('@pubsweet/logger')

const isFromServer = operation =>
  ['create', 'read', 'update', 'delete'].includes(operation)

// Helper functions
const getUser = async (userId, operation, context) => {
  const id = getId(userId)
  const fromServer = isFromServer(operation)
  let user

  if (fromServer) {
    user = await context.models.User.find(id, {
      eager: 'teams.members.[user]',
    })
  } else {
    user = await context.models.User.find(id)
  }

  return user
}

const isTeamMember = async (user, roles, object, context, operation) => {
  const teams = await Promise.all(
    user.teams.map(teamId => context.models.Team.find(getId(teamId))),
  )

  const rolesArray = Array.isArray(roles) ? roles : [roles]
  const objectId = isFromServer(operation) ? 'objectId' : 'object.objectId'

  const team = teams.find(
    aTeam =>
      !aTeam.global &&
      object.id === get(aTeam, objectId) &&
      rolesArray.includes(aTeam.role),
  )

  return !!team
}

const isGlobalTeamMember = async (user, roles, context) => {
  const teams = await Promise.all(
    user.teams.map(teamId => context.models.Team.find(getId(teamId))),
  )

  const rolesArray = Array.isArray(roles) ? roles : [roles]

  const team = teams.find(
    aTeam => aTeam.global && rolesArray.includes(aTeam.role),
  )

  return !!team
}

const isEditor = (user, context) =>
  isGlobalTeamMember(user, ['editors'], context)

const isGlobal = (user, context) =>
  isGlobalTeamMember(user, ['editors', 'scienceOfficers'], context)

const isAuthor = async (user, object, context, operation) =>
  isTeamMember(user, 'author', object, context, operation)

const isInvitedReviewer = (user, object, context, operation) =>
  isTeamMember(user, 'reviewersInvited', object, context, operation)

const updatedProperties = (current, update) => {
  const diff = Object.keys(current).filter(k => {
    if (!update[k]) return false

    if (typeof current[k] === 'string') {
      return current[k] !== update[k]
    }
    return !isEqual(current[k], update[k])
  })
  return diff
}

const arrayContains = (superset, subset) =>
  difference(subset, superset).length === 0

const getId = objectOrString => {
  // In the browser client, ids are sometimes in an object, this is a way
  // to address that difference

  if (typeof objectOrString === 'string') {
    return objectOrString
  }
  return objectOrString.id
}

const permissions = {
  // eslint-disable-next-line consistent-return
  before: async (userId, operation, object, context) => {
    if (
      userId === undefined &&
      (operation === 'create' || operation === 'read') &&
      (object === 'User' || object.type === 'user')
    ) {
      return true // it's a signup operation
    }

    const user = await getUser(userId, operation, context)
    if (user.admin) return true
  },
  create: (userId, operation, object, context) => true,
  read: async (userId, operation, object, context) => {
    const user = await getUser(userId, operation, context)

    // Everyone can list the manuscripts
    if (object === 'Manuscript') {
      return true
    }

    // Only the author can see an unsubmitted manuscript
    if (object && object.type === 'manuscript') {
      if (object.status) {
        if (
          !object.status.submission.full &&
          !object.status.submission.initial
        ) {
          return isTeamMember(user, 'author', object, context, operation)
        } else if (object.status.submission.initial) {
          const isAuthorMember = await isAuthor(
            user,
            object,
            context,
            operation,
          )
          const isGlobalMember = await isGlobal(user, context)
          return isAuthorMember || isGlobalMember
        }
      }
    }

    // Everyone can read Teams
    if (object === 'Team') {
      return true
    }

    if (object && object.type === 'team') {
      return true
    }

    // Everyone can read Users
    if (object === 'User') {
      return true
    }

    if (object && object.type === 'user') {
      return true
    }

    if (object === 'TeamMember') {
      return true
    }

    // Capture team member (has no type property)
    // if (object && object.type === 'teamMember') {
    if (object && object.teamId && object.userId) {
      return true
    }

    if (object === 'Review') return true

    if (object && object.type === 'review') {
      const global = await isGlobal(user, context)
      const isUsersReview = userId === object.reviewerId

      return isUsersReview || global
    }

    return false
  },
  update: async (userId, operation, object, context) => {
    const user = await getUser(userId, operation, context)
    if (!user) return false

    // Everyone can update the manuscripts, in principle
    if (object === 'Manuscript') return true

    if (
      object.current &&
      object.current.type === 'manuscript' &&
      object.update
    ) {
      const { current, update } = object
      const initial = get(current, 'status.submission.initial')
      const full = get(current, 'status.submission.full')

      const changedFields = updatedProperties(current, update)

      // Nobody should be able to update the dataType before initial submission
      if (update.dataType !== current.dataType) {
        if (!full && !initial) return false
      }

      if (isGlobal(user, context)) {
        const editorWhitelist = [
          'currentlyWith',
          'dataType',
          'decisionLetter',
          'doi',
          'status',
        ]

        // Allow editors to change fields in their whitelist
        // (only after initial submission)
        if (
          initial &&
          // update.dataType !== current.dataType &&
          arrayContains(editorWhitelist, changedFields)
        ) {
          // return isGlobal(user, context)
          return true
        }
      }

      if (isAuthor(user, object, context, operation)) {
        // WARNING: remove status
        const authorWhitelist = [
          'acknowledgements',
          'affiliations',
          'authors',
          'comments',
          'disclaimer',
          'funding',
          'geneExpression',
          'image',
          'imageCaption',
          'laboratory',
          'methods',
          'reagents',
          'patternDescription',
          'references',
          'suggestedReviewer',
          'title',
          'status',
        ]

        if (
          arrayContains(authorWhitelist, updatedProperties(current, update))
        ) {
          return isTeamMember(user, 'author', current, context, operation)
        }
      }
    }

    if (object === 'Team') return true

    if (get(object, 'current.type') === 'team') {
      const { current, update } = object
      const role = get(object, 'current.role')
      const changed = Object.keys(update)

      // No one can update something other than members on existing teams
      if (!isEqual(changed, ['members'])) return false
      const affectedIds = xor(current.members, update.members)

      // Only global users can update the editor & SO team members for an object
      const global = isGlobal(user, context)
      if (role === 'editor' || role === 'scienceOfficer') return global

      // Only editors can update the reviewer teams
      const editor = isEditor(user, context)
      const editorAllow = ['reviewers', 'reviewersInvited']
      if (editor && editorAllow.includes(role)) return true

      // Only invited reviewers can alter accepted or rejected teams
      // They can only apply changes that affect themselves and no one else
      if (!isEqual(affectedIds, [userId])) return false
      const reviewerInvited = await isInvitedReviewer(
        user,
        { id: current.object.objectId }, // pass article as object, not team
        // { id: current.objectId }, // pass article as object, not team
        context,
        operation,
      )
      const reviewerAllow = ['reviewersAccepted', 'reviewersRejected']
      if (reviewerAllow.includes(role)) return reviewerInvited

      // DEPRECATED -- safe to remove?
      // Necessary, as any user needs to run the normalize team mutation
      return true
    }

    if (object === 'Review') return true

    if (get(object, 'current.type') === 'review') {
      const { current, update } = object

      // Cannot update someone else's review
      if (current.reviewerId !== userId) return false

      // Can only update these fields
      const whiteList = [
        'content',
        'events',
        'openAcknowledgement',
        'recommendation',
        'status',
        'askedToSeeRevision',
      ]
      const changedFields = Object.keys(update)
      const updateAllowedByWhitelist = arrayContains(whiteList, changedFields)
      return updateAllowedByWhitelist
    }

    return false
  },
}

module.exports = permissions
