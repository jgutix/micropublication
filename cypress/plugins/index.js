// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const { Manuscript, Team, TeamMember } = require('@pubsweet/models')

module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config

  on('task', {
    async clearDb() {
      await Team.query()
        .delete()
        .whereNot({ global: true })
        .orWhere({ global: null })

      const globalTeamMembers = await TeamMember.query()
        .eager('team')
        .modifyEager('team', builder => builder.where({ global: true }))
      const globalTeamMemberIds = globalTeamMembers.map(i => i.id)

      await TeamMember.query()
        .delete()
        .whereNotIn('id', globalTeamMemberIds)

      await Manuscript.query().delete()

      return null
    },

    // async seedUsers() {
    //   const users = [
    //     {
    //       email: 'author@example.com',
    //       password: 'password',
    //       username: 'author',
    //     },
    //     {
    //       email: 'editor@example.com',
    //       password: 'password',
    //       username: 'editor',
    //     },
    //     {
    //       email: 'scienceOfficer@example.com',
    //       password: 'password',
    //       username: 'scienceOfficer',
    //     },
    //     {
    //       email: 'reviewer@example.com',
    //       password: 'password',
    //       username: 'reviewer',
    //     },
    //   ]

    //   // logger.info('>>>>> Creating test users')

    //   const newUsers = users.map(
    //     user =>
    //       new User({
    //         ...user,
    //       }),
    //   )

    //   await Promise.all(newUsers.map(u => u.save()))
    //   // .then(res => {
    //   //   logger.info('>>>>> All test users successfully created')
    //   // })
    //   return null
    // },
  })
}
