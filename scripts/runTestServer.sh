NODE_ENV='test'

. ./scripts/resetDb.sh
node ./scripts/seedTeams.js
node ./scripts/seedTestUsers.js

pubsweet server
