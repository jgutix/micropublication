#! /bin/sh

# Check that pubsweet is installed
if [ -f "./node_modules/.bin/pubsweet" ]
then
  PUBSWEET='./node_modules/.bin/pubsweet'
else
  echo 'Pubsweet is not installed. Make sure you have installed the application dependencies.'
  exit 1
fi

# Default to development environment
NODE_ENV=${NODE_ENV:-development}
echo "environment is set as $NODE_ENV"

# Load environment variables
if [ -f "./config/$NODE_ENV.env" ]
then
  echo "loading $NODE_ENV.env file"
  . ./config/$NODE_ENV.env
fi

# Build setupdb options
# [[ -v CLOBBER ]] && CL="--clobber"
# [[ -v ADMIN_USERNAME ]] && AU="--username $ADMIN_USERNAME"
# [[ -v ADMIN_PASSWORD ]] && AP="--password $ADMIN_PASSWORD"
# [[ -v ADMIN_EMAIL ]] && AE="--email $ADMIN_EMAIL"

# $PUBSWEET setupdb $CL $AU $AP $AE

$PUBSWEET migrate

NODE_ENV=$NODE_ENV \
ADMIN_USERNAME=$ADMIN_USERNAME \
ADMIN_PASSWORD=$ADMIN_PASSWORD \
ADMIN_EMAIL=$ADMIN_EMAIL \
ADMIN_GIVEN_NAME=$ADMIN_GIVEN_NAME \
ADMIN_SURNAME=$ADMIN_SURNAME \
node './scripts/setupAdminUser.js'

node './scripts/seedTeams.js'
