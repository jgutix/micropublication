/**
 * Deletes all non-user data
 * Useful for demo instances where we might wanna clear manuscripts etc,
 * without needing all users to sign up again.
 */

const { transaction } = require('objection')
const logger = require('@pubsweet/logger')

const {
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
} = require('@pubsweet/models')

const clearDemo = async () => {
  logger.info('Clearing demo data')

  try {
    await transaction(Manuscript.knex(), async trx => {
      logger.info('Transaction started')

      // Delete all non-global teams & team membeships
      await TeamMember.query(trx)
        .leftJoin('teams', 'team_id', 'teams.id')
        .whereIn('team_id', builder => {
          builder
            .select('id')
            .from('teams')
            .where({ global: false })
        })
        .delete()

      await Team.query(trx)
        .where({ global: false })
        .delete()

      // // Delete all manuscripts and reviews
      await ManuscriptVersion.query(trx).delete()
      await Manuscript.query(trx).delete()
      await Review.query(trx).delete()
    })

    logger.info('Data successfully cleared')
  } catch (e) {
    logger.error('Something went wrong. Rolling back...')
    throw new Error(e)
  }
}

clearDemo()
