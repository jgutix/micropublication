const {
  ChatMessage,
  ChatThread,
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
  User,
} = require('@pubsweet/models')

const clear = async () => {
  await TeamMember.query().delete()
  await Team.query().delete()
  await ManuscriptVersion.query().delete()
  await Review.query().delete()
  await ChatMessage.query().delete()
  await ChatThread.query().delete()
  await Manuscript.query().delete()
  await User.query().delete()
}

module.exports = clear
