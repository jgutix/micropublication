exports.up = knex =>
  knex.schema.table('manuscript_versions', table => {
    table
      .boolean('submitted')
      .defaultTo(false)
      .notNullable()
  })
