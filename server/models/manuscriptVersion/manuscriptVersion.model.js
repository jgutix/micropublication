const clone = require('lodash/clone')
const omit = require('lodash/omit')
const { transaction } = require('objection')

const BaseModel = require('@pubsweet/base-model')
const { Team, TeamMember } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const baseMessage = 'Manuscript Version Model:'

const {
  arrayOfObjectsNullable,
  boolean,
  booleanNullable,
  id,
  object,
  objectNullable,
  string,
  stringNotEmpty,
  stringNullable,
} = require('../_helpers/types')

class ManuscriptVersion extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'manuscriptVersion'
  }

  static get tableName() {
    return 'manuscriptVersions'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['manuscriptId'],
      properties: {
        manuscriptId: id,
        active: booleanNullable,
        submitted: boolean,
        isApprovedByScienceOfficer: booleanNullable,

        acknowledgements: stringNullable,
        authors: arrayOfObjectsNullable,
        comments: stringNullable,
        disclaimer: booleanNullable,
        funding: stringNullable,
        image: objectNullable,
        imageCaption: stringNullable,
        laboratory: objectNullable,
        methods: stringNullable,
        reagents: stringNullable,
        patternDescription: stringNullable,
        references: arrayOfObjectsNullable,
        suggestedReviewer: objectNullable,
        title: stringNullable,

        dataTypeFormData: object,

        decision: string,
        decisionLetter: stringNotEmpty,
      },
    }
  }

  /* 
    Transaction (trx) is optional.
    If no transaction is given then one is created.
    If one is given, then all queries will run in the context of the given
    transaction.
  */
  async makeNewVersion(trx) {
    const newVersionData = omit(clone(this), [
      'id',
      'created',
      'updated',
      'decision',
      'decisionLetter',
      'submitted',
      'isApprovedByScienceOfficer',
    ])

    newVersionData.active = true

    const makeChanges = async trxFinal => {
      await this.$query(trxFinal).patch({
        active: null,
      })

      const newVersion = await ManuscriptVersion.query(trxFinal).insert(
        newVersionData,
      )

      const teamsToCreate = [
        {
          name: `author-${newVersion.id}`,
          objectId: newVersion.id,
          objectType: 'manuscriptVersion',
          role: 'author',
        },
        {
          name: `reviewer-${newVersion.id}`,
          objectId: newVersion.id,
          objectType: 'manuscriptVersion',
          role: 'reviewer',
        },
      ]

      const newVersionTeams = await Team.query(trxFinal).insert(teamsToCreate)

      const authorTeam = await Team.query(trxFinal).findOne({
        objectId: this.id,
        role: 'author',
      })

      const authorMembers = await TeamMember.query(trxFinal).where({
        teamId: authorTeam.id,
      })

      const newAuthorTeam = newVersionTeams.find(t => t.role === 'author')

      const membersToCreate = authorMembers.map(a => ({
        teamId: newAuthorTeam.id,
        userId: a.userId,
      }))

      await TeamMember.query(trxFinal).insert(membersToCreate)

      return newVersion
    }

    if (!trx) {
      try {
        const changes = await transaction(
          ManuscriptVersion.knex(),
          async trxNew => makeChanges(trxNew),
        )
        return changes
      } catch (e) {
        logger.error(
          `${baseMessage} Make new version: Transaction failed! Rolling back...`,
        )
        throw new Error(e)
      }
    } else {
      return makeChanges(trx)
    }
  }
}

module.exports = ManuscriptVersion
