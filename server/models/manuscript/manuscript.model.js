const BaseModel = require('@pubsweet/base-model')
const { Team } = require('@pubsweet/models')

const { boolean, id, object, string } = require('../_helpers/types')

class Manuscript extends BaseModel {
  static get tableName() {
    return 'manuscripts'
  }

  constructor(properties) {
    super(properties)
    this.type = 'manuscript'
  }

  static get schema() {
    return {
      properties: {
        currentlyWith: id,
        data: object,
        dataType: string,
        doi: string,
        isDataTypeSelected: boolean,
        isInitiallySubmitted: boolean,
      },
    }
  }

  async $beforeDelete() {
    await Team.deleteAssociated(this.data.type, this.id)
  }
}

module.exports = Manuscript
