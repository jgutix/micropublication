const BaseModel = require('@pubsweet/base-model')

const { id } = require('../_helpers/types')

class ChatThread extends BaseModel {
  constructor(properties) {
    super(properties)
    this.type = 'chatThread'
  }

  static get tableName() {
    return 'chatThreads'
  }

  static get schema() {
    return {
      type: 'object',
      required: ['chatType', 'manuscriptId'],
      properties: {
        chatType: {
          type: 'string',
          enum: ['scienceOfficer', 'reviewer', 'author'],
        },
        manuscriptId: id,
        reviewerId: id,
      },
    }
  }

  static get relationMappings() {
    /* eslint-disable-next-line global-require */
    const { ChatMessage } = require('@pubsweet/models')

    return {
      messages: {
        relation: BaseModel.HasManyRelation,
        modelClass: ChatMessage,
        join: {
          from: 'chatMessages.chatThreadId',
          to: 'chatThreads.id',
        },
      },
    }
  }
}

module.exports = ChatThread
