const { TeamMember, User } = require('@pubsweet/models')
const uniq = require('lodash/uniq')

const { REVIEWER_STATUSES } = require('../../api/constants')

/* 
  HELPERS
*/

const manuscriptVersionFromMemberQuery = () =>
  TeamMember.query()
    .select(
      'team_members.*',
      'teams.role',
      'manuscripts.id as manuscriptId',
      'manuscript_versions.id as manuscriptVersionId',
    )
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .leftJoin(
      'manuscript_versions',
      'teams.object_id',
      'manuscript_versions.id',
    )
    .leftJoin(
      'manuscripts',
      'manuscript_versions.manuscript_id',
      'manuscripts.id',
    )

const manuscriptFromMemberQuery = () =>
  TeamMember.query()
    .select('team_members.*', 'teams.role', 'manuscripts.id as manuscriptId')
    .leftJoin('teams', 'team_members.team_id', 'teams.id')
    .leftJoin('manuscripts', 'teams.object_id', 'manuscripts.id')

const teamFromMemberQuery = () =>
  TeamMember.query()
    .select('team_members.*', 'teams.global', 'teams.role')
    .leftJoin('teams', 'team_members.team_id', 'teams.id')

/* 
  END HELPERS
*/

const createClientAuth = async userId => {
  const user = await User.query().findById(userId)

  /* 
    Is user member of any global team
  */
  const globalMembership = await teamFromMemberQuery().where({
    global: true,
    userId,
  })

  const isGlobal = globalMembership.length > 0 || user.admin
  const isGlobalEditor = !!globalMembership.find(t => t.role === 'editors')
  const isGlobalScienceOfficer = !!globalMembership.find(
    t => t.role === 'scienceOfficers',
  )
  const isGlobalCurator = !!globalMembership.find(
    t => t.role === 'globalCurator',
  )
  const isGlobalSectionEditor = !!globalMembership.find(
    t => t.role === 'globalSectionEditor',
  )

  /* 
    Manuscripts that the user is an author of
  */
  const authorManuscripts = await manuscriptVersionFromMemberQuery().where({
    role: 'author',
    userId,
  })

  const isAuthor = authorManuscripts.map(m => m.manuscriptId)

  /*
    Manuscripts that the user is assigned to as a science officer
  */

  const scienceOfficerManuscripts = await manuscriptFromMemberQuery().where({
    role: 'scienceOfficer',
    userId,
  })

  const isAssignedScienceOfficer = scienceOfficerManuscripts.map(
    m => m.manuscriptId,
  )

  /*
    Manuscripts that the user is assigned to as an editor
  */

  const editorManuscripts = await manuscriptFromMemberQuery().where({
    role: 'editor',
    userId,
  })

  const isAssignedEditor = editorManuscripts.map(m => m.manuscriptId)

  /*
    Manuscripts that the user is assigned to as a curator
  */

  const curatorManuscripts = await manuscriptFromMemberQuery().where({
    role: 'curator',
    userId,
  })

  const isAssignedCurator = curatorManuscripts.map(m => m.manuscriptId)

  /*
    Manuscripts (not versions) that the user has accepted an invitation
    to review
  */
  const acceptedReviewInvitationManuscripts = await manuscriptVersionFromMemberQuery().where(
    {
      role: 'reviewer',
      'team_members.status': REVIEWER_STATUSES.accepted,
      userId,
    },
  )

  const isAcceptedReviewerForManuscript = uniq(
    acceptedReviewInvitationManuscripts.map(m => m.manuscriptId),
  )

  const isAcceptedReviewerForVersion = acceptedReviewInvitationManuscripts.map(
    m => m.manuscriptVersionId,
  )

  return {
    isAcceptedReviewerForManuscript,
    isAcceptedReviewerForVersion,
    isAssignedCurator,
    isAssignedEditor,
    isAssignedScienceOfficer,
    isAuthor,
    isGlobal,
    isGlobalCurator,
    isGlobalEditor,
    isGlobalScienceOfficer,
    isGlobalSectionEditor,
  }
}

module.exports = createClientAuth
