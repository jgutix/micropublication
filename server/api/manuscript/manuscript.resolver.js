const { raw, transaction } = require('objection')
const reverse = require('lodash/reverse')
const sortBy = require('lodash/sortBy')

const {
  ChatThread,
  ChatMessage,
  Manuscript,
  ManuscriptVersion,
  Review,
  Team,
  TeamMember,
} = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

const { REVIEWER_STATUSES } = require('../constants')
const { notify } = require('../../services')

const baseMessage = 'Manuscript resolver:'

const createManuscript = async (_, variables, ctx) => {
  const { user: userId } = ctx

  try {
    let manuscript

    await transaction(Manuscript.knex(), async trx => {
      manuscript = await Manuscript.query(trx).insert({})

      // Create first version
      const version = await ManuscriptVersion.query(trx).insert({
        active: true,
        manuscriptId: manuscript.id,
      })

      // Create SO & author chat threads
      await ChatThread.query(trx).insert({
        chatType: 'scienceOfficer',
        manuscriptId: manuscript.id,
      })

      await ChatThread.query(trx).insert({
        chatType: 'author',
        manuscriptId: manuscript.id,
      })

      // TO DO -- change objectType to manuscript
      const teamData = [
        // Teams on the manuscript
        {
          name: `editor-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'editor',
        },
        {
          name: `science-officer-${manuscript.id}`,
          objectId: manuscript.id,
          objectType: 'article',
          role: 'scienceOfficer',
        },
        // Teams on the version
        {
          name: `author-${version.id}`,
          objectId: version.id,
          objectType: 'manuscriptVersion',
          role: 'author',
        },
        {
          name: `reviewer-${version.id}`,
          objectId: version.id,
          objectType: 'manuscriptVersion',
          role: 'reviewer',
        },
      ]

      const teams = await Team.query(trx).insert(teamData)

      // Add author to author manuscript team
      const authorTeam = teams.find(t => t.role === 'author')

      await TeamMember.query(trx).insert({
        teamId: authorTeam.id,
        userId,
      })
    })

    return manuscript.id
  } catch (e) {
    logger.error(`${baseMessage} Create new manuscript failed! Rolling back...`)
    throw new Error(e)
  }
}

const deleteManuscript = async (_, { id }) => {
  try {
    await transaction(Manuscript.knex(), async trx => {
      // Delete chat thread and their messages
      const threads = await ChatThread.query(trx)
        .delete()
        .where({ manuscriptId: id })
        .returning('id')

      const threadIds = threads.map(thread => thread.id)

      await ChatMessage.query(trx)
        .delete()
        .whereIn('chatThreadId', threadIds)

      // Delete versions
      const deletedVersions = await ManuscriptVersion.query(trx)
        .delete()
        .where({ manuscriptId: id })
        .returning('id')

      const deletedVersionIds = deletedVersions.map(v => v.id)

      // Delete all teams and team members that belong to the manuscript,
      // its versions or its chat threads
      const deletedTeams = await Team.query(trx)
        .delete()
        .whereIn('objectId', [...deletedVersionIds, id])
        .returning('id')

      const deletedTeamIds = deletedTeams.map(t => t.id)

      await TeamMember.query(trx)
        .delete()
        .whereIn('teamId', deletedTeamIds)

      // Finally, delete the manuscript itself
      await Manuscript.query(trx).deleteById(id)
    })

    return id
  } catch (e) {
    logger.error('Delete manuscript: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

const handleInvitation = async (_, { action, articleVersionId }, ctx) => {
  const userId = ctx.user
  const manuscriptVersionId = articleVersionId

  if (action !== 'accept' && action !== 'reject')
    throw new Error(
      `Invalid action provided to handleInvitation:
       Must be either "accept" or "reject"`,
    )

  try {
    await transaction(Team.knex(), async trx => {
      const reviewerTeamForVersion = await Team.query(trx).findOne({
        objectId: manuscriptVersionId,
        role: 'reviewer',
      })

      if (!reviewerTeamForVersion)
        throw new Error(
          `Handle invitation: No reviewer team found for version ${manuscriptVersionId}`,
        )

      let status
      if (action === 'accept') status = REVIEWER_STATUSES.accepted
      if (action === 'reject') status = REVIEWER_STATUSES.rejected

      await TeamMember.query(trx)
        .patch({ status })
        .where({
          teamId: reviewerTeamForVersion.id,
          userId,
        })

      await Review.query(trx).insert({
        articleVersionId: manuscriptVersionId,
        reviewerId: userId,
        status: {
          pending: true,
          submitted: false,
        },
      })

      if (action === 'accept') {
        const manuscriptVersion = await ManuscriptVersion.query(trx).findById(
          manuscriptVersionId,
        )
        const { manuscriptId } = manuscriptVersion

        const queryData = {
          chatType: 'reviewer',
          manuscriptId,
          reviewerId: userId,
        }

        let reviewerChat = await ChatThread.query(trx).findOne(queryData)

        if (!reviewerChat) {
          reviewerChat = await ChatThread.query(trx).insert(queryData)
        }
      }
    })

    notify('reviewerInvitationResponse', {
      action,
      userId,
      versionId: manuscriptVersionId,
    })

    return true
  } catch (e) {
    logger.error(e.stack)
    throw new Error(e)
  }
}

// TO DO -- throw if not found
const manuscript = async (_, { id }, ctx) => Manuscript.query().findById(id)

const manuscripts = async (_, { role }, ctx) => {
  const userId = ctx.user
  const sortResult = result => reverse(sortBy(result, 'updated'))

  const baseQuery = () =>
    Manuscript.query()
      .select(
        raw('distinct on (manuscripts.id) manuscript_id'),
        'manuscripts.*',
        'members.status',
      )
      .leftJoin(
        'manuscript_versions as versions',
        'manuscripts.id',
        'versions.manuscript_id',
      )
      .leftJoin('teams', 'versions.id', 'teams.object_id')
      .leftJoin('team_members as members', 'members.team_id', 'teams.id')
      .leftJoin('users', 'users.id', 'members.user_id')
  // .debug()
  // .orderBy('updated', 'desc')

  if (role === 'author') {
    const res = await baseQuery().where({
      role,
      userId,
    })

    return sortResult(res)
  }

  if (role === 'reviewer') {
    const res = await baseQuery()
      .where({
        role,
        userId,
      })
      .whereNot({
        // Reviewers should only see manuscripts when they're invited
        'members.status': REVIEWER_STATUSES.added,
      })

    return sortResult(res)
  }

  if (role === 'editor') {
    /* 
      Editors should NOT be able to 
      - manage their own manuscripts
      - see manuscripts that haven't been submitted yet
    */
    const res = await baseQuery()
      .where({ role: 'author' })
      .whereNot({
        isInitiallySubmitted: false,
        userId,
      })

    return sortResult(res)
  }

  if (role === 'scienceOfficer') {
    const res = await Manuscript.query()
      .leftJoin('teams', 'manuscripts.id', 'teams.object_id')
      .leftJoin('team_members as members', 'members.team_id', 'teams.id')
      .leftJoin('users', 'users.id', 'members.user_id')
      .where({ role, userId })
    // .debug()

    return sortResult(res)
  }

  if (role === 'sectionEditor') {
    const res = await Manuscript.query()
      .leftJoin('teams', 'manuscripts.id', 'teams.object_id')
      .leftJoin('team_members as members', 'members.team_id', 'teams.id')
      .leftJoin('users', 'users.id', 'members.user_id')
      .where({
        role: 'editor',
        userId,
      })

    return sortResult(res)
  }

  return null
}

const updateManuscriptMetadata = async (_, { data, manuscriptId }, ctx) => {
  try {
    const { doi } = data
    await Manuscript.query().patch({ doi })
    return manuscriptId
  } catch (e) {
    logger.error('Update metadata: Update failed!')
    throw new Error(e)
  }
}

const setDataType = async (_, { manuscriptId, input }, ctx) => {
  const { user: userId } = ctx
  const { dataType } = input
  let updated

  try {
    await transaction(Manuscript.knex(), async trx => {
      const manuscriptItem = await Manuscript.query(trx).findById(manuscriptId)

      if (manuscriptItem.isDataTypeSelected)
        throw new Error('Datatype has already been set')

      updated = await manuscriptItem.$query(trx).patchAndFetch({
        dataType,
        isDataTypeSelected: true,
      })

      if (dataType === 'noDatatype') {
        await ManuscriptVersion.query(trx)
          .patch({ submitted: true })
          .findOne({
            active: true,
            manuscriptId,
          })
      }
    })

    notify('dataTypeSelected', { manuscript: updated, userId })
    return updated.id
  } catch (e) {
    logger.error('Set datatype: Transaction failed! Rolling back...')
    throw new Error(e)
  }
}

module.exports = {
  createManuscript,
  deleteManuscript,
  handleInvitation,
  manuscript,
  updateManuscriptMetadata,
  manuscripts,
  setDataType,
}
