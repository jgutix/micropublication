const map = require('lodash/map')
const longest = require('longest')
const rightPad = require('right-pad')
const conventionalCommitTypes = require('conventional-commit-types')

/* 
  Borrowed from cz-conventional-changelog
*/
/* eslint-disable prefer-arrow-callback, func-names, prefer-template */
const { types } = conventionalCommitTypes
const length = longest(Object.keys(types)).length + 1
const choices = map(types, function(type, key) {
  return {
    name: rightPad(key + ':', length) + ' ' + type.description,
    value: key,
  }
})
/* eslint-enable prefer-arrow-callback, func-names, prefer-template */

module.exports = {
  scopes: [
    'dashboard',
    'submission form',
    'editor panel',
    'reviewer panel',
    'assign reviewers',
    'preview',
    'team manager',
    'user profile',
    'api',
    'models',
    'notifications',
    'client',
    'server',
    'config',
    '*',
  ],
  skipQuestions: ['body', 'breaking', 'footer'],
  types: choices,
}

/* 
  Possible new types
*/
// types: [
//   {
//     value: 'content',
//     name: 'content:   Changes to the text displayed in the UI, emails, etc',
//   },
//   {
//     value: 'tweak',
//     name: 'tweak:     Minor UI changes',
//   },
// ],
