import React from 'react'
import { addDecorator } from '@storybook/react'
import { createGlobalStyle, ThemeProvider } from 'styled-components'
import { Normalize } from 'styled-normalize'

import { th } from '@pubsweet/ui-toolkit'

import theme from '../app/theme'

const GlobalStyle = createGlobalStyle`
  body {
    background-color: ${th('colorBackground')};
    color: ${th('colorText')};
    font-family: ${th('fontInterface')}, sans-serif;
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};

    * {
      box-sizing: border-box;
    }

    p {
      margin: 0;
    }
  }
`

addDecorator(storyFn => (
  <ThemeProvider theme={theme}>
    <>
      <Normalize />
      <GlobalStyle />
      {storyFn()}
    </>
  </ThemeProvider>
))
