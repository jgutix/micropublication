/* eslint-disable react/prop-types */

import React from 'react'
import styled from 'styled-components'
import { isArray, clone } from 'lodash'
import * as yup from 'yup'
import { v4 as uuid } from 'uuid'

import { stripHTML } from '../_helpers'
import { Button, Form, List } from '../common'
import PanelTextEditor from './PanelTextEditor'

const Wrapper = styled.div``

const Editor = styled(PanelTextEditor)`
  box-sizing: border-box;
`

const transformEntries = entries =>
  entries.map(entry => ({
    id: entry.id || uuid(),
    label: entry.displayName,
    readOnly: true,
    value: entry.content,
  }))

/*
  HACK
  In order for the editor content to be reset, we need to destroy the xpub-edit
  instance and create a new one. We only need to reset the value on submit. So
  this is a counter that is incremented on every submit.
*/
let key = 0

const initialValues = { content: '' }

const validations = yup.object().shape({
  content: yup
    .string()
    .test(
      'chat-message-not-empty',
      'Chat message is required',
      val => stripHTML(val).length > 0,
    ),
})

const NewEntry = ({ sendMessage }) => {
  const handleSubmit = (values, formikBag) => {
    const { content } = values
    const { resetForm, setStatus } = formikBag

    sendMessage(content).then(() => {
      resetForm()
      setStatus({ hasReset: true })
    })
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
    >
      {formProps => {
        const { isValid, setStatus, status, values } = formProps

        // See note on key above
        if (status && status.hasReset) {
          key += 1
          setStatus({ hasReset: false })
        }

        return (
          <>
            <Editor
              key={key}
              name="content"
              placeholder="Make a comment"
              value={values.content}
              {...formProps}
            />

            <Button disabled={!isValid} primary type="submit">
              Send
            </Button>
          </>
        )
      }}
    </Form>
  )
}

const Chat = props => {
  const { messages, sendMessage } = props

  let entries = clone(messages)
  const hasEntries = isArray(entries) && entries.length > 0
  if (hasEntries) entries = transformEntries(entries)

  return (
    <Wrapper>
      {hasEntries && <List component={Editor} items={entries} />}
      <NewEntry sendMessage={sendMessage} />
    </Wrapper>
  )
}

export default Chat
