import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { th } from '../_helpers'

const statuses = {
  success: ['success', 'accept'],
  error: ['error', 'reject'],
  warning: ['warning', 'revise'],
  primary: ['primary'],
}

const StyledStatus = styled.span`
  color: ${props => {
    const { status } = props
    if (statuses.success.includes(status)) return th('colorSuccess')
    if (statuses.error.includes(status)) return th('colorError')
    if (statuses.warning.includes(status)) return th('colorWarning')
    if (statuses.primary.includes(status)) return th('colorPrimary')
    return th('colorText')
  }};
  font-family: ${th('fontInterface')};
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  text-transform: uppercase;
`

const Status = props => {
  const { children, status } = props
  return <StyledStatus status={status}>{children}</StyledStatus>
}

Status.propTypes = {
  status: PropTypes.oneOf([
    'success',
    'error',
    'warning',
    'accept',
    'reject',
    'revise',
    'primary',
  ]),
}

Status.defaultProps = {
  status: null,
}

export default Status
