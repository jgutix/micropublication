import React from 'react'
import PropTypes from 'prop-types'
import styled, { withTheme } from 'styled-components'
import { debounce, get, isEqual } from 'lodash'
import * as yup from 'yup'

import {
  Button,
  Checkbox,
  DiscreetButton,
  Form,
  Radio,
  Status,
  TextEditor,
} from '../common'

const Wrapper = styled.div``

const ChatWrapper = styled.div`
  /* align-items: flex-end; */
  display: flex;
  justify-content: flex-end;
`

const Editor = styled(TextEditor)`
  min-height: 50vh;
  width: 100%;
`

const StyledRadio = styled(Radio)`
  width: 100%;
`

const validations = yup.object().shape({
  content: yup.string().required('Cannot leave review empty'),
  recommendation: yup.string().required('You need to make a recommendation'),
  openAcknowledgement: yup.boolean(),
  askedToSeeRevision: yup.boolean(),
})

const makeOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Accept version',
    value: 'accept',
  },
  {
    color: theme.colorWarning,
    label: 'Needs revision',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'Reject article',
    value: 'reject',
  },
]

/* eslint-disable react/prop-types */
class AutoSave extends React.Component {
  componentWillReceiveProps(nextProps, nextContext) {
    if (!isEqual(nextProps.values, this.props.values)) {
      this.save()
    }
  }

  save = debounce(() => {
    const { values } = this.props
    Promise.resolve(this.props.onSave(values))
  }, 500)

  render() {
    return null
  }
}
/* eslint-enable react/prop-types */

const ReviewerPanel = props => {
  const {
    decision,
    onClickChat,
    review,
    save,
    submit,
    submitted,
    theme,
  } = props
  const decisionExists = !!decision

  if (decisionExists && !submitted)
    return (
      <Wrapper>
        <Status status="error">
          You did not submit a review for this version
        </Status>
      </Wrapper>
    )

  const initialValues = {
    content: get(review, 'content') || '',
    recommendation: get(review, 'recommendation') || '',
    openAcknowledgement: get(review, 'openAcknowledgement') || false,
    askedToSeeRevision: get(review, 'askedToSeeRevision') || false,
  }

  const handleSubmit = values => submit(values)
  const radioOptions = makeOptions(theme)

  return (
    <Wrapper>
      <ChatWrapper>
        <DiscreetButton onClick={onClickChat}>
          chat with the editors
        </DiscreetButton>
      </ChatWrapper>

      {submitted && (
        <div>
          <Status status="primary">Submitted</Status>
        </div>
      )}

      {submitted && decisionExists && (
        <Status>
          editor decision: <Status status={decision}>{decision}</Status>
        </Status>
      )}

      <Form
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            handleChange,
            setFieldTouched,
            setFieldValue,
            touched,
            values,
          } = formProps

          return (
            <>
              <Editor
                bold
                error={errors.content}
                italic
                key={submitted}
                label="Review text"
                name="content"
                placeholder="Write your review"
                readOnly={submitted}
                required
                setFieldTouched={setFieldTouched}
                setFieldValue={setFieldValue}
                subscript
                superscript
                touched={touched}
                value={values.content}
              />

              <StyledRadio
                error={errors.recommendation}
                inline
                label="Recommendation to the Editors"
                name="recommendation"
                options={radioOptions}
                readOnly={submitted}
                required
                setFieldValue={setFieldValue}
                theme={theme}
                touched={touched}
                value={values.recommendation}
              />

              <Checkbox
                checkBoxText="I would like to be openly acknowledged as the reviewer"
                checked={values.openAcknowledgement}
                name="openAcknowledgement"
                onChange={handleChange}
                readOnly={submitted}
                setFieldTouched={setFieldTouched}
                touched={touched}
              />

              <Checkbox
                checkBoxText="I would like to see the revised article"
                checked={values.askedToSeeRevision}
                name="askedToSeeRevision"
                onChange={handleChange}
                readOnly={submitted}
                setFieldTouched={setFieldTouched}
                touched={touched}
              />

              {!submitted && (
                <Button primary type="submit">
                  Submit
                </Button>
              )}

              <AutoSave onSave={save} values={values} />
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

const validDecisions = ['accept', 'reject', 'revise']

ReviewerPanel.propTypes = {
  decision: PropTypes.oneOf(validDecisions),
  onClickChat: PropTypes.func.isRequired,
  review: PropTypes.shape({
    content: PropTypes.string,
    recommendation: PropTypes.oneOf(validDecisions),
    openAcknowledgement: PropTypes.bool,
    askedToSeeRevision: PropTypes.bool,
  }),
  save: PropTypes.func,
  submit: PropTypes.func,
  submitted: PropTypes.bool,
}

ReviewerPanel.defaultProps = {
  decision: null,
  review: null,
  save: null,
  submit: null,
  submitted: false,
}

export default withTheme(ReviewerPanel)
