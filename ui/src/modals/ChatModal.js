/* eslint-disable react/prop-types */
import React from 'react'

import { Chat } from '../common'
import Modal from './Modal'
import ModalHeader from './ModalHeader'

const ChatModal = props => {
  const { headerText, messages, sendMessage, ...rest } = props

  const Header = <ModalHeader text={headerText || 'Chat'} />

  return (
    <Modal headerComponent={Header} size="large" {...rest}>
      <Chat messages={messages} sendMessage={sendMessage} />
    </Modal>
  )
}

export default ChatModal
