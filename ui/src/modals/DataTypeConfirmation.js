import React from 'react'
import PropTypes from 'prop-types'
import WarningModal from './WarningModal'

const DataTypeConfirmation = props => {
  const { dataTypeLabel, ...rest } = props

  if (!dataTypeLabel) return null

  return (
    <WarningModal
      headerText="Confirm your datatype selection"
      textSuccess="set datatype"
      {...rest}
    >
      Are you sure you want to set &quot;{dataTypeLabel}&quot; as the datatype
      for this manuscript?
    </WarningModal>
  )
}

DataTypeConfirmation.propTypes = {
  dataTypeLabel: PropTypes.string.isRequired,
}

export default DataTypeConfirmation
