import React from 'react'

import WarningModal from './WarningModal'

const ReviewerCoiModal = props => (
  <WarningModal
    headerText="Confirm review submission"
    justifyText
    textSuccess="submit"
    {...props}
  >
    Are you sure you want to submit your review?
  </WarningModal>
)

export default ReviewerCoiModal
