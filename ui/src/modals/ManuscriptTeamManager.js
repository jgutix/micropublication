/* eslint-disable react/prop-types */

import React from 'react'
// import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid, th } from '../_helpers'
import { Form, Ribbon, Select } from '../common'

import Modal from './Modal'
import ModalHeader from './ModalHeader'
import ModalFooterDialog from './ModalFooterDialog'

const StyledModal = styled(Modal)`
  .Modal {
    /* height: 750px; */
    height: 80vh;
    left: 50%;
    margin-left: -400px;
    /* margin-top: -400px; */
    margin-top: -40vh;
    top: 50%;
    width: 800px;
  }
`

const SelectWrapper = styled.div`
  margin: 0 auto;
  max-width: 500px;

  > div:not(:last-child) {
    margin-bottom: ${grid(3)};
  }
`

const Label = styled.div`
  align-self: flex-end;
  color: ${th('colorPrimary')};
  padding: 10px 0 6px 0;
  text-transform: uppercase;
`

const RoleWrapper = styled.div``

const warningMessage = `
  Clicking on "Update Team" will affect who has access to this article.
`

const Header = <ModalHeader text="Article Team Manager" />

const Footer = props => {
  const { onConfirm, onRequestClose } = props
  return (
    <ModalFooterDialog
      onConfirm={onConfirm}
      onRequestClose={onRequestClose}
      textSuccess="Update Team"
    />
  )
}

const userToValue = user => ({
  label: user.displayName,
  isDisabled: user.isDisabled,
  value: user.id,
})

const Role = props => {
  const { label, name, options, setFieldValue, value } = props

  const selectOptions = options ? options.map(userToValue) : []

  const handleChange = selected => {
    if (!selected) {
      setFieldValue(name, null)
    } else {
      const option = options.find(item => item.id === selected.value)
      setFieldValue(name, option)
    }
  }

  return (
    <RoleWrapper>
      <Label>{label}:</Label>
      <Select
        // menuIsOpen
        isClearable
        onChange={handleChange}
        options={selectOptions}
        value={value && userToValue(value)}
      />
    </RoleWrapper>
  )
}

const ManuscriptTeamManager = props => {
  const {
    assignedCurator,
    assignedEditor,
    assignedScienceOfficer,
    // curators,
    editors,
    scienceOfficers,
    updateTeams,
    ...rest
  } = props

  const initialValues = {
    // curator: assignedCurator || null,
    editor: assignedEditor || null,
    scienceOfficer: assignedScienceOfficer || null,
  }

  const handleSubmit = values => {
    updateTeams(values).then(() => {
      props.onRequestClose()
    })
  }

  return (
    <Form initialValues={initialValues} onSubmit={handleSubmit}>
      {formProps => {
        const { setFieldValue, submitForm, values } = formProps

        const ModalFooter = (
          <Footer
            onConfirm={submitForm}
            onRequestClose={props.onRequestClose}
          />
        )

        return (
          <StyledModal
            footerComponent={ModalFooter}
            headerComponent={Header}
            {...rest}
          >
            <Ribbon message={warningMessage} status="warning" />

            <SelectWrapper>
              <Role
                label="editor"
                name="editor"
                options={editors}
                setFieldValue={setFieldValue}
                value={values.editor}
              />

              <Role
                label="science officer"
                name="scienceOfficer"
                options={scienceOfficers}
                setFieldValue={setFieldValue}
                value={values.scienceOfficer}
              />

              {/* <Role
                label="curator"
                name="curator"
                options={curators}
                setFieldValue={setFieldValue}
              /> */}
            </SelectWrapper>
          </StyledModal>
        )
      }}
    </Form>
  )
}

// ManuscriptTeamManager.propTypes = {}

// ManuscriptTeamManager.defaultProps = {}

export default ManuscriptTeamManager
