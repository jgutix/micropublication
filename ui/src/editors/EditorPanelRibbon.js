import React from 'react'
import PropTypes from 'prop-types'

import { Ribbon } from '../common'

const options = {
  default: {
    message: 'Datatype selection pending',
    status: null,
  },
  decisionPending: {
    message: 'Editor decision pending',
    status: null,
  },
  accept: {
    message: 'This article has been accepted',
    status: 'success',
  },
  reject: {
    message: 'This article has been rejected',
    status: 'error',
  },
  revise: {
    message: 'This version was marked for revision',
    status: 'warning',
  },
}

const EditorPanelRibbon = props => {
  const { dataType, decision } = props

  const type = dataType ? decision || 'decisionPending' : 'default'
  const { message, status } = options[type]

  return <Ribbon message={message} status={status} />
}

EditorPanelRibbon.propTypes = {
  dataType: PropTypes.string,
  decision: PropTypes.oneOf(['accept', 'reject', 'revise']),
}

EditorPanelRibbon.defaultProps = {
  dataType: null,
  decision: null,
}

export default EditorPanelRibbon
