import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { grid } from '../_helpers'
import { Accordion as AccordionBase, Chat } from '../common'

import DataType from './DataType'
import DecisionSection from './DecisionSection'
import EditorPanelRibbon from './EditorPanelRibbon'
import InfoSection from './InfoSection'
import MetadataSection from './MetadataSection'
import ReviewsSection from './ReviewsSection'

const Wrapper = styled.div`
  padding: ${grid(1)} 0;
`

const AccordionContent = styled.div`
  margin: ${grid(1)} 0 ${grid(1)} ${grid(4)};
`

const Accordion = props => {
  /* eslint-disable-next-line react/prop-types */
  const { children, label, startExpanded } = props

  return (
    <AccordionBase label={label} startExpanded={startExpanded}>
      <AccordionContent>{children}</AccordionContent>
    </AccordionBase>
  )
}

const EditorPanel = props => {
  const {
    acceptedReviewersCount,
    invitedReviewersCount,
    rejectedReviewersCount,
    authorEmail,
    authorName,
    dataType,
    decision,
    decisionLetter,
    decisionSubmitted,
    doi,
    editorName,
    onClickReviewerChat,
    onClickReviewerReinvite,
    onClickSetDataType,
    onClickTeamManager,
    reviewersFromPreviousVersions,
    reviewersPageUrl,
    reviews,
    scienceOfficerChatMessages,
    scienceOfficerName,
    sendScienceOfficerChatMessage,
    showDataType,
    showDataTypeSelection,
    showDecision,
    showInfo,
    showManageReviewers,
    showMetadata,
    showPreviousReviewers,
    showReviewersChat,
    showReviews,
    showScienceOfficerChat,
    showTeamManager,
    startChatExpanded,
    startDataTypeExpanded,
    startDecisionExpanded,
    startMetadataExpanded,
    startReviewsExpanded,
    submitDecision,
    updateMetadata,
  } = props

  return (
    <Wrapper>
      <EditorPanelRibbon dataType={dataType} decision={decision} />

      {showInfo && (
        <InfoSection
          authorEmail={authorEmail}
          authorName={authorName}
          editorName={editorName}
          onClickTeamManager={onClickTeamManager}
          scienceOfficerName={scienceOfficerName}
          showTeamManager={showTeamManager}
        />
      )}

      {showDataType && (
        <Accordion label="Datatype" startExpanded={startDataTypeExpanded}>
          <DataType
            dataType={dataType}
            onClickSetDataType={onClickSetDataType}
            showDataTypeSelection={showDataTypeSelection}
          />
        </Accordion>
      )}

      {showScienceOfficerChat && (
        <Accordion
          label="Chat / Suggest Reviewer"
          startExpanded={startChatExpanded}
        >
          <Chat
            messages={scienceOfficerChatMessages}
            sendMessage={sendScienceOfficerChatMessage}
          />
        </Accordion>
      )}

      {showReviews && (
        <Accordion label="Reviews" startExpanded={startReviewsExpanded}>
          <ReviewsSection
            acceptedReviewersCount={acceptedReviewersCount}
            invitedReviewersCount={invitedReviewersCount}
            onClickChat={onClickReviewerChat}
            onClickInvite={onClickReviewerReinvite}
            rejectedReviewersCount={rejectedReviewersCount}
            reviewersFromPreviousVersions={reviewersFromPreviousVersions}
            reviewersPageUrl={reviewersPageUrl}
            reviews={reviews}
            showChat={showReviewersChat}
            showManageReviewers={showManageReviewers}
            showPreviousReviewers={showPreviousReviewers}
          />
        </Accordion>
      )}

      {showDecision && (
        <Accordion label="Decision" startExpanded={startDecisionExpanded}>
          <DecisionSection
            decision={decision}
            decisionLetter={decisionLetter}
            submitDecision={submitDecision}
            submitted={decisionSubmitted}
          />
        </Accordion>
      )}

      {showMetadata && (
        <Accordion label="Metadata" startExpanded={startMetadataExpanded}>
          <MetadataSection doi={doi} updateMetadata={updateMetadata} />
        </Accordion>
      )}
    </Wrapper>
  )
}

const validDecisions = ['accept', 'reject', 'revise']

EditorPanel.propTypes = {
  /** Datatype selected for this submission */
  dataType: PropTypes.string,

  /** How many reviewers have been invited so far */
  invitedReviewersCount: PropTypes.number.isRequired,
  /** How many reviewers have accepted their invitation so far */
  acceptedReviewersCount: PropTypes.number.isRequired,
  /** How many reviewers have rejected their invitation so far */
  rejectedReviewersCount: PropTypes.number.isRequired,

  /** Decision that has been taken on this version */
  decision: PropTypes.oneOf(validDecisions),
  /** The decision letter that was sent to the author */
  decisionLetter: PropTypes.string,
  /** Whether the decision has been sent (as opposed to simply saved) */
  decisionSubmitted: PropTypes.bool,

  /** DOI given to the manuscript */
  doi: PropTypes.string,

  /** Submitting author's email */
  authorEmail: PropTypes.string,
  /** Submitting author's display name */
  authorName: PropTypes.string,
  /** Assigned editor's display name */
  editorName: PropTypes.string,
  /** Assigned science officer's display name */
  scienceOfficerName: PropTypes.string,

  /** Function to run on clicking 'chat with reviewer' */
  onClickReviewerChat: PropTypes.func,
  /** Function to run on clicking 'reinvite reviewer' */
  onClickReviewerReinvite: PropTypes.func,
  /** Function to run on clicking 'set datatype' */
  onClickSetDataType: PropTypes.func,
  /** Function to run on clicking 'team manager' */
  onClickTeamManager: PropTypes.func,
  /** Function to run on clicking 'send to author' in the decision section */
  submitDecision: PropTypes.func,
  /** Function to run on clicking 'update metadata' */
  updateMetadata: PropTypes.func,

  /** List of reviewers that existed on previous versions */
  reviewersFromPreviousVersions: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      displayName: PropTypes.string,
      recommendation: PropTypes.oneOf(validDecisions),
    }),
  ),

  /** URL of 'manage reviewers' page */
  reviewersPageUrl: PropTypes.string,

  /** Pending or submitted reviews for this version */
  reviews: PropTypes.arrayOf(
    PropTypes.shape({
      askedToSeeRevision: PropTypes.bool,
      content: PropTypes.string,
      openAcknowledgement: PropTypes.bool,
      pending: PropTypes.bool,
      recommendation: PropTypes.oneOf(validDecisions),
      reviewerId: PropTypes.string,
      reviewerName: PropTypes.string,
    }),
  ),

  /** Science officer chat messages */
  scienceOfficerChatMessages: PropTypes.arrayOf(
    PropTypes.shape({
      displayName: PropTypes.string.isRequired,
      content: PropTypes.string.isRequired,
    }),
  ),

  /** Function to run on clicking 'send' in SO chat */
  sendScienceOfficerChatMessage: PropTypes.func,

  /** Control whether the info section at the top is shown or not */
  showInfo: PropTypes.bool,
  /** Control whether to show the datatype section */
  showDataType: PropTypes.bool,
  /** Control whether the science officer chat is shown or not */
  showScienceOfficerChat: PropTypes.bool,
  /** Control whether the reviews section is shown or not */
  showReviews: PropTypes.bool,
  /** Control whether the decision section is shown or not */
  showDecision: PropTypes.bool,
  /** Control whether the metadata section is shown or not */
  showMetadata: PropTypes.bool,

  /** Control whether to show the datatype dropdown when there is no value */
  showDataTypeSelection: PropTypes.bool,

  /** Control whether to show the previous reviewers list in the reviewers section */
  showPreviousReviewers: PropTypes.bool,
  /** Control whether the 'chat with reviewer' buttons in the reviews section are shown or not */
  showReviewersChat: PropTypes.bool,
  /** Control whether the 'Manage reviewers' link is displayed in the reviews section */
  showManageReviewers: PropTypes.bool,
  /** Control whether the 'manage team' button in the info section is shown or not */
  showTeamManager: PropTypes.bool,

  /** Control whether the science officer chat should start expanded */
  startChatExpanded: PropTypes.bool,
  /** Control whether the datatype section should start expanded */
  startDataTypeExpanded: PropTypes.bool,
  /** Control whether the reviews section should start expanded */
  startReviewsExpanded: PropTypes.bool,
  /** Control whether the decision section should start expanded */
  startDecisionExpanded: PropTypes.bool,
  /** Contro whether the metadata section should start expanded */
  startMetadataExpanded: PropTypes.bool,
}

EditorPanel.defaultProps = {
  authorEmail: null,
  authorName: null,
  dataType: null,
  decision: null,
  decisionLetter: null,
  decisionSubmitted: false,
  doi: null,
  editorName: null,
  onClickReviewerChat: null,
  onClickReviewerReinvite: null,
  onClickSetDataType: null,
  onClickTeamManager: null,
  reviewersFromPreviousVersions: [],
  reviewersPageUrl: null,
  reviews: [],
  scienceOfficerName: null,
  scienceOfficerChatMessages: [],
  sendScienceOfficerChatMessage: null,
  showDataType: false,
  showDataTypeSelection: false,
  showDecision: false,
  showInfo: false,
  showManageReviewers: false,
  showMetadata: false,
  showPreviousReviewers: false,
  showReviewersChat: false,
  showReviews: false,
  showScienceOfficerChat: false,
  showTeamManager: false,
  startChatExpanded: false,
  startDataTypeExpanded: false,
  startDecisionExpanded: false,
  startMetadataExpanded: false,
  startReviewsExpanded: false,
  submitDecision: null,
  updateMetadata: null,
}

export default EditorPanel
