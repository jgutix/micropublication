import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled, { css, ThemeContext, withTheme } from 'styled-components'
import * as yup from 'yup'

import { stripHTML } from '../_helpers'
import { Button, Form, PanelTextEditor, Radio, Status } from '../common'

const Wrapper = styled.div``

const Editor = styled(PanelTextEditor)`
  div[contenteditable] {
    ${props =>
      props.readOnly &&
      css`
        border-bottom: 0;
      `};
  }
`

const validations = yup.object().shape({
  decision: yup.string().required('You need to make a decision'),
  decisionLetter: yup
    .string()
    .test(
      'decision-letter-not-empty',
      'You need to write a decision letter',
      val => {
        if (!val) return false
        return stripHTML(val).length > 0
      },
    ),
})

const makeOptions = theme => [
  {
    color: theme.colorSuccess,
    label: 'Accept',
    value: 'accept',
  },
  {
    color: theme.colorWarning,
    label: 'Revise',
    value: 'revise',
  },
  {
    color: theme.colorError,
    label: 'Reject',
    value: 'reject',
  },
]

const DecisionSection = props => {
  const { decision, decisionLetter, submitDecision, submitted } = props

  const theme = useContext(ThemeContext)
  const radioOptions = makeOptions(theme)

  const initialValues = {
    decision: decision || '',
    decisionLetter: decisionLetter || '',
  }

  const handleSubmit = (values, formikBag) => {
    submitDecision({
      decision: values.decision,
      decisionLetter: values.decisionLetter,
    })
  }

  return (
    <Wrapper>
      <Form
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validationSchema={validations}
      >
        {formProps => {
          const {
            errors,
            setFieldTouched,
            setFieldValue,
            touched,
            values,
          } = formProps

          return (
            <>
              {submitted && (
                <>
                  <Status status="primary">decision submitted</Status>: &nbsp;
                  <Status status={decision}>{decision}</Status>
                </>
              )}

              {!submitted && (
                <Radio
                  error={errors.decision}
                  inline
                  label="Decision"
                  name="decision"
                  options={radioOptions}
                  readOnly={submitted}
                  required={!submitted}
                  setFieldValue={setFieldValue}
                  touched={touched}
                  values={values}
                />
              )}

              <Editor
                error={errors.decisionLetter}
                key={submitted}
                label="Decision letter"
                name="decisionLetter"
                placeholder="Make some comments to the author"
                readOnly={submitted}
                required={!submitted}
                setFieldTouched={setFieldTouched}
                setFieldValue={setFieldValue}
                touched={touched}
                value={values.decisionLetter}
              />

              {!submitted && (
                <Button primary type="submit">
                  Send to Author
                </Button>
              )}
            </>
          )
        }}
      </Form>
    </Wrapper>
  )
}

DecisionSection.propTypes = {
  decision: PropTypes.oneOf(['accept', 'reject', 'revise']),
  decisionLetter: PropTypes.string,
  submitDecision: PropTypes.func,
  submitted: PropTypes.bool,
}

DecisionSection.defaultProps = {
  decision: null,
  decisionLetter: null,
  submitDecision: null,
  submitted: false,
}

export default withTheme(DecisionSection)
