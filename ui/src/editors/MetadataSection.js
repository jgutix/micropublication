import React, { useState } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import * as yup from 'yup'

import { grid } from '../_helpers'
import { Button, Form, TextField } from '../common'
import ValueList from './ValueList'

const Wrapper = styled.div``

const MetadataValues = styled(ValueList)`
  margin-bottom: ${grid(2)};
`

const validations = yup.object().shape({
  doi: yup.string().required('DOI cannot be empty'),
})

const MetadataSection = props => {
  const { doi, updateMetadata } = props
  const [showForm, setShowForm] = useState(false)

  /**
   * Either value list
   */
  const data = [
    {
      label: 'DOI',
      status: 'primary',
      value: doi,
    },
  ]

  if (!showForm)
    return (
      <Wrapper>
        <MetadataValues data={data} />

        <div>
          <Button onClick={() => setShowForm(true)} primary>
            Edit
          </Button>
        </div>
      </Wrapper>
    )

  /**
   * Or form
   */
  const initialValues = {
    doi: doi || '',
  }

  const handleSubmit = (values, formikBag) => {
    updateMetadata(values).then(() => setShowForm(false))
  }

  return (
    <Form
      initialValues={initialValues}
      onSubmit={handleSubmit}
      validationSchema={validations}
    >
      {formProps => {
        const { errors, handleBlur, handleChange, touched, values } = formProps

        return (
          <Wrapper>
            <TextField
              error={errors.doi}
              handleBlur={handleBlur}
              handleChange={handleChange}
              label="DOI"
              name="doi"
              touched={touched}
              value={values.doi}
            />

            <Button onClick={() => setShowForm(false)}>Cancel</Button>
            <Button primary type="submit">
              Save
            </Button>
          </Wrapper>
        )
      }}
    </Form>
  )
}

MetadataSection.propTypes = {
  doi: PropTypes.string,
  updateMetadata: PropTypes.func.isRequired,
}

MetadataSection.defaultProps = {
  doi: null,
}

export default MetadataSection
