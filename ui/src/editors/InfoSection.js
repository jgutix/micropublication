import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import { Button } from '../common'
import ValueList from './ValueList'

const Wrapper = styled.div`
  display: flex;
  width: 100%;
`

const TeamManagerWrapper = styled.div`
  align-items: flex-end;
  display: flex;
  flex-grow: 1;
  justify-content: flex-end;
`

const InfoSection = props => {
  const {
    authorEmail,
    authorName,
    editorName,
    scienceOfficerName,
    onClickTeamManager,
    showTeamManager,
  } = props

  const teamValues = [
    {
      label: 'Editor',
      status: 'primary',
      value: editorName,
    },
    {
      label: 'Science Officer',
      status: 'primary',
      value: scienceOfficerName,
    },
    {
      label: 'Submitting Author',
      status: 'primary',
      value: `${authorName} ( ${authorEmail} )`,
    },
  ]

  return (
    <Wrapper>
      <ValueList data={teamValues} missingValueText="not assigned" />

      {showTeamManager && (
        <TeamManagerWrapper>
          <Button onClick={onClickTeamManager} primary>
            Manage Team
          </Button>
        </TeamManagerWrapper>
      )}
    </Wrapper>
  )
}

InfoSection.propTypes = {
  authorEmail: PropTypes.string.isRequired,
  authorName: PropTypes.string.isRequired,
  editorName: PropTypes.string,
  scienceOfficerName: PropTypes.string,
  onClickTeamManager: PropTypes.func,
  showTeamManager: PropTypes.bool,
}

InfoSection.defaultProps = {
  editorName: null,
  scienceOfficerName: null,
  onClickTeamManager: null,
  showTeamManager: false,
}

export default InfoSection
