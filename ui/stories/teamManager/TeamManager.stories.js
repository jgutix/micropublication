import React from 'react'
import { name, random } from 'faker'
import { range } from 'lodash'

import TeamManager from '../../src/teamManager/TeamManager'

const users = range(10).map(() => ({
  displayName: name.findName(),
  id: random.uuid(),
}))

const teams = [
  {
    name: 'Editors',
    role: 'editors',
    members: users.slice(0, 2),
  },
  {
    name: 'Science Officers',
    role: 'scienceOfficers',
    members: users.slice(2, 3),
  },
]

const updateFn = data => new Promise(resolve => resolve(true))

export const Base = () => (
  <TeamManager teams={teams} updateGlobalTeamMembers={updateFn} users={users} />
)

export default {
  component: TeamManager,
  title: 'Team Manager/Team Manager',
}
