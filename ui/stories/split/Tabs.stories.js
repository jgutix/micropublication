import React, { useState } from 'react'
import styled from 'styled-components'

import { th } from '@pubsweet/ui-toolkit'

import Tabs from '../../src/split/Tabs'

const ContentComponent = styled.div`
  background: ${props =>
    props.primary ? th('colorPrimary') : th('colorBackground')};
  border: 1px solid
    ${props => (props.primary ? th('colorBackground') : th('colorPrimary'))};
  color: ${props =>
    props.primary ? th('colorTextReverse') : th('colorPrimary')};
  height: 200px;
  padding-top: 90px;
  text-align: center;
  width: 800px;
`

const sections = [
  {
    key: '1',
    label: 'First',
    content: <ContentComponent>something</ContentComponent>,
  },
  {
    key: '2',
    label: 'Second',
    content: <ContentComponent primary>something else</ContentComponent>,
  },
]

export const Base = () => <Tabs sections={sections} />

export const ActiveKey = () => <Tabs activeKey="2" sections={sections} />

ActiveKey.story = {
  parameters: {
    docs: {
      storyDescription: 'Use activeKey to control which tab is pre-selected',
    },
  },
}

export const ExternalControl = () => {
  const [activeKey, setActiveKey] = useState('1')

  return (
    <>
      <div>
        External controls say active tab is &quot;
        {sections.find(s => s.key === activeKey).label}&quot;
      </div>

      <Tabs
        activeKey={activeKey}
        alwaysUseActiveKeyFromProps
        onChange={key => setActiveKey(key)}
        sections={sections}
      />
    </>
  )
}

ExternalControl.story = {
  name: 'Control active key from outside component',
  parameters: {
    docs: {
      storyDescription: `You can also skip the component's internal state completely and control which tab is active only through props`,
    },
  },
}

export default {
  component: Tabs,
  title: 'Article/Tabs',
}
