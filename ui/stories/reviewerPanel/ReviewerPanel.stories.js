import React, { useState } from 'react'
import styled from 'styled-components'
import { lorem, random } from 'faker'

import ReviewerPanel from '../../src/reviewerPanel/ReviewerPanel'

const Demo = styled.div`
  border-bottom: 2px solid gray;
  margin-bottom: 20px;
  padding-bottom: 10px;
`

export const Base = () => {
  const [review, setReview] = useState(null)
  const [submitted, setSubmitted] = useState(false)

  const save = values => {
    console.log('autosave running', values)
  }

  const submit = values => {
    console.log('submitting!', values)
    setReview(values)
    setSubmitted(true)
  }

  const reset = () => {
    setReview(null)
    setSubmitted(false)
  }

  return (
    <>
      <Demo>
        For demo purposes only:
        <button disabled={!submitted} onClick={reset}>
          Unsubmit form
        </button>
      </Demo>

      <ReviewerPanel
        onClickChat={() => console.log('open chat')}
        review={review}
        save={save}
        submit={submit}
        submitted={submitted}
      />
    </>
  )
}

const validRecommendations = ['accept', 'reject', 'revise']

const review = {
  content: lorem.sentences(30),
  recommendation: random.arrayElement(validRecommendations),
  openAcknowledgement: random.boolean(),
  askedToSeeRevision: random.boolean(),
}

export const Submitted = () => <ReviewerPanel review={review} submitted />

export const SubmittedAndDecisionExists = () => (
  <ReviewerPanel decision="revise" review={review} submitted />
)

export const TooLateToSubmit = () => (
  <ReviewerPanel decision="revise" submtited={false} />
)

export default {
  component: ReviewerPanel,
  title: 'Reviewer Panel/Reviewer Panel',
}
