import React from 'react'
import { name, random } from 'faker'

import ReinviteReviewer from '../../src/editors/ReinviteReviewer'

export const Base = () => (
  <ReinviteReviewer
    displayName={name.findName()}
    id={random.uuid()}
    onClickInvite={id => console.log('invite', id)}
    recommendation={random.arrayElement(['accept', 'reject', 'revise'])}
  />
)

export default {
  component: ReinviteReviewer,
  title: 'Editors/Reinvite Reviewer',
}
