// import React, { useState } from 'react'
// import { name, lorem } from 'faker'
// import { range } from 'lodash'

// import ScienceOfficerChat from '../../src/editors/ScienceOfficerChat'

// const makeMessages = () =>
//   range(4).map(() => ({
//     displayName: name.findName(),
//     content: lorem.sentences(3),
//   }))

// export const Base = () => {
//   const initialMessages = makeMessages()

//   const [messages, setMessages] = useState(initialMessages)

//   const sendMessage = message => {
//     setMessages(
//       messages.concat({
//         displayName: 'Fake user',
//         content: message,
//       }),
//     )

//     return Promise.resolve(true)
//   }

//   return <ScienceOfficerChat messages={messages} sendMessage={sendMessage} />
// }

// export default {
//   component: ScienceOfficerChat,
//   title: 'Editors/Science Officer Chat',
// }
