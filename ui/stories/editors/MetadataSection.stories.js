import React, { useState } from 'react'

import MetadataSection from '../../src/editors/MetadataSection'

export const Base = () => {
  const [doi, setDoi] = useState('743748923748239477238947923')
  // const [doi, setDoi] = useState(null)

  const update = values => {
    setDoi(values.doi)
    return Promise.resolve()
  }

  return <MetadataSection doi={doi} updateMetadata={update} />
}

export default {
  component: MetadataSection,
  title: 'Editors/Metadata Section',
}
