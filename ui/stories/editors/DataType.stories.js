import React, { useState } from 'react'
import styled from 'styled-components'

import Datatype from '../../src/editors/DataType'

const Wrapper = styled.div`
  height: 250px;
`

const Demo = styled.div`
  border-bottom: 2px solid gray;
  margin-bottom: 30px;
  padding-bottom: 10px;
`

export const Base = () => {
  const [dataType, setDataType] = useState(null)

  const handleClick = valueObject => setDataType(valueObject.value)

  return (
    <Wrapper>
      <Demo>
        For demo purposes only:{' '}
        <button onClick={() => setDataType(null)}>Reset</button>
      </Demo>

      <Datatype
        dataType={dataType}
        onClickSetDataType={handleClick}
        showDataTypeSelection
      />
    </Wrapper>
  )
}

export const HideSelect = () => <Datatype />

export const WithValue = () => <Datatype dataType="noDatatype" />

export default {
  component: Datatype,
  title: 'Editors/Datatype',
}
