import React, { useState } from 'react'
import styled from 'styled-components'
import { lorem, name, random } from 'faker'
import { range } from 'lodash'

import ReviewsSection from '../../src/editors/ReviewsSection'

const DemoWrapper = styled.div`
  border-bottom: 2px solid gray;
  margin-bottom: 10px;
`

const Btn = styled.button`
  margin-bottom: 10px;
`

const recommendations = ['accept', 'reject', 'revise']

const makeReviews = amount =>
  range(amount).map(() => ({
    askedToSeeRevision: random.boolean(),
    content: lorem.sentences(20),
    openAcknowledgement: random.boolean(),
    pending: false,
    recommendation: random.arrayElement(recommendations),
    reviewerId: random.uuid(),
    reviewerName: name.findName(),
  }))

const makePreviousReviewers = amount =>
  range(amount).map(() => ({
    id: random.uuid(),
    displayName: name.findName(),
    recommendation: random.arrayElement(recommendations),
  }))

export const Base = () => {
  const [invited, setInvited] = useState(2)
  const [reviewers, setReviewers] = useState(makePreviousReviewers(3))

  const reinviteReviewer = reviewerId => {
    setInvited(invited + 1)
    setReviewers(reviewers.filter(reviewer => reviewer.id !== reviewerId))
    return Promise.resolve()
  }

  const reset = () => {
    setInvited(2)
    setReviewers(makePreviousReviewers(3))
  }

  return (
    <>
      <DemoWrapper>
        For demo purposes only: <Btn onClick={reset}>Reset invitations</Btn>
      </DemoWrapper>

      <ReviewsSection
        acceptedReviewersCount={2}
        invitedReviewersCount={invited}
        onClickChat={id => console.log('chat', id)}
        onClickInvite={reinviteReviewer}
        rejectedReviewersCount={0}
        reviewersFromPreviousVersions={reviewers}
        reviewersPageUrl={null}
        reviews={makeReviews(2)}
        showChat
        showManageReviewers
        showPreviousReviewers
      />
    </>
  )
}

export const RestrictedForPreviousVersions = () => (
  <ReviewsSection
    acceptedReviewersCount={2}
    invitedReviewersCount={2}
    rejectedReviewersCount={0}
    reviewersPageUrl={null}
    reviews={makeReviews(2)}
    showChat
  />
)

RestrictedForPreviousVersions.story = {
  parameters: {
    docs: {
      storyDescription:
        "Previous versions don't have 'reinvite reviewers' list, a link to the page to manage reviewers or a ribbon for feedback, as that is associated with the action of reinviting a reviewer.",
    },
  },
}

export const RestrictedForScienceOfficer = () => (
  <ReviewsSection
    acceptedReviewersCount={2}
    invitedReviewersCount={2}
    rejectedReviewersCount={0}
    reviewersPageUrl={null}
    reviews={makeReviews(2)}
  />
)

RestrictedForScienceOfficer.story = {
  parameters: {
    docs: {
      storyDescription:
        'All the restrictions of previous versions apply to science officers as well, with the addition that they cannot chat to reviewers.',
    },
  },
}

export default {
  component: ReviewsSection,
  title: 'Editors/Reviews Section',
}
