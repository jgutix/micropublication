import React from 'react'
import { internet, name } from 'faker'

import InfoSection from '../../src/editors/InfoSection'

export const Base = () => (
  <InfoSection
    authorEmail={internet.exampleEmail()}
    authorName={name.findName()}
    editorName={name.findName()}
    onClickTeamManager={() => console.log('clicked team manager')}
    scienceOfficerName={name.findName()}
    showTeamManager
  />
)

export const WithMissingValues = () => (
  <InfoSection
    authorEmail={internet.exampleEmail()}
    authorName={name.findName()}
    editorName={name.findName()}
    onClickTeamManager={() => console.log('clicked team manager')}
    showTeamManager
  />
)

export const WithTeamManagerButtonHidden = () => (
  <InfoSection
    authorEmail={internet.exampleEmail()}
    authorName={name.findName()}
    editorName={name.findName()}
  />
)

export default {
  component: InfoSection,
  title: 'Editors/Info Section',
}
