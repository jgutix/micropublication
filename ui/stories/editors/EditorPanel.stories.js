import React, { useState } from 'react'
import { internet, lorem, name, random } from 'faker'
import { range } from 'lodash'

import EditorPanel from '../../src/editors/EditorPanel'

const recommendations = ['accept', 'reject', 'revise']

const makeMessages = () =>
  range(4).map(() => ({
    displayName: name.findName(),
    content: lorem.sentences(3),
  }))

const makeReviewers = amount =>
  range(amount).map(() => ({
    id: random.uuid(),
    displayName: name.findName(),
    recommendation: random.arrayElement(recommendations),
  }))

const makeReviews = amount =>
  range(amount).map(() => ({
    askedToSeeRevision: random.boolean(),
    content: lorem.sentences(10),
    openAcknowledgement: random.boolean(),
    pending: false,
    recommendation: random.arrayElement(recommendations),
    reviewerId: random.uuid(),
    reviewerName: name.findName(),
  }))

const authorEmail = internet.exampleEmail()
const authorName = name.findName()
const editorName = name.findName()
const scienceOfficerName = name.findName()

export const Base = () => {
  const initialMessages = makeMessages()

  const [soMessages, setSoMessages] = useState(initialMessages)

  const sendSoMessage = message => {
    setSoMessages(
      soMessages.concat({
        displayName: 'Fake user',
        content: message,
      }),
    )

    return Promise.resolve(true)
  }

  return (
    <EditorPanel
      acceptedReviewersCount={2}
      authorEmail={authorEmail}
      authorName={authorName}
      // dataType="noDatatype"
      decision={null}
      decisionLetter={null}
      decisionSubmitted={false}
      doi="5734057289703"
      editorName={editorName}
      invitedReviewersCount={2}
      onClickReviewerChat={id => console.log('chat', id)}
      onClickReviewerReinvite={id => console.log('reinvite', id)}
      onClickSetDataType={value => console.log('set datatype', value)}
      onClickTeamManager={() => console.log('clicked team manager')}
      rejectedReviewersCount={0}
      reviewersFromPreviousVersions={makeReviewers(3)}
      reviewersPageUrl={null}
      reviews={makeReviews(2)}
      scienceOfficerChatMessages={soMessages}
      scienceOfficerName={scienceOfficerName}
      sendScienceOfficerChatMessage={sendSoMessage}
      showDataType
      showDataTypeSelection
      showDecision
      showInfo
      showManageReviewers
      showMetadata
      showPreviousReviewers
      showReviewersChat
      showReviews
      showScienceOfficerChat
      showTeamManager
      // startChatExpanded
      startDataTypeExpanded
      // startDecisionExpanded
      // startMetadataExpanded
      // startReviewsExpanded
      submitDecision={values => console.log(values)}
    />
  )
}

export const ScenarioPreviousVersion = () => {
  const initialMessages = makeMessages()

  const [soMessages, setSoMessages] = useState(initialMessages)

  const sendSoMessage = message => {
    setSoMessages(
      soMessages.concat({
        displayName: 'Fake user',
        content: message,
      }),
    )

    return Promise.resolve(true)
  }

  return (
    <EditorPanel
      acceptedReviewersCount={2}
      authorEmail={authorEmail}
      authorName={authorName}
      dataType="noDatatype"
      decision="revise"
      decisionLetter={lorem.sentences(10)}
      decisionSubmitted
      doi="5734057289703"
      editorName={editorName}
      invitedReviewersCount={2}
      onClickReviewerChat={id => console.log('chat', id)}
      onClickReviewerReinvite={id => console.log('reinvite', id)}
      onClickTeamManager={() => console.log('clicked team manager')}
      rejectedReviewersCount={0}
      reviewersFromPreviousVersions={makeReviewers(3)}
      reviewersPageUrl={null}
      reviews={makeReviews(2)}
      scienceOfficerChatMessages={soMessages}
      scienceOfficerName={scienceOfficerName}
      sendScienceOfficerChatMessage={sendSoMessage}
      showDecision
      showReviewersChat
      showReviews
    />
  )
}

export default {
  component: EditorPanel,
  title: 'Editors/Editor Panel',
}
