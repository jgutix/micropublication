import React from 'react'
import { name, lorem, random } from 'faker'
import { range } from 'lodash'

import ReviewList from '../../src/editors/ReviewList'

const makeReviews = amount =>
  range(amount).map(() => ({
    askedToSeeRevision: random.boolean(),
    content: lorem.sentences(20),
    openAcknowledgement: random.boolean(),
    // pending: random.boolean(),
    pending: false,
    recommendation: random.arrayElement(['accept', 'reject', 'revise']),
    reviewerId: random.uuid(),
    reviewerName: name.findName(),
  }))

export const Base = () => <ReviewList reviews={makeReviews(2)} showChat />

export const Empty = () => <ReviewList />

export default {
  component: ReviewList,
  title: 'Editors/Review List',
}
