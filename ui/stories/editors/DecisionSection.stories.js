import React from 'react'
import { lorem } from 'faker'

import DecisionSection from '../../src/editors/DecisionSection'

export const Base = () => (
  <DecisionSection submitDecision={values => console.log(values)} />
)

export const Submitted = () => (
  <DecisionSection
    decision="revise"
    decisionLetter={lorem.sentences(10)}
    submitted
  />
)

export default {
  component: DecisionSection,
  title: 'Editors/Decision Section',
}
