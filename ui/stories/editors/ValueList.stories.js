import React from 'react'
import { lorem, random } from 'faker'
import { range } from 'lodash'

import ValueList from '../../src/editors/ValueList'

const makeValues = (amount, useStatus) =>
  range(amount).map(() => ({
    label: lorem.word(),
    status: useStatus
      ? random.arrayElement(['primary', 'success', 'warning', 'error'])
      : null,
    value: lorem.words(2),
  }))

export const Base = () => <ValueList data={makeValues(3)} />

export const WithStatus = () => <ValueList data={makeValues(3, true)} />

export const WithMissingValue = () => {
  const data = makeValues(3)
  data[1].value = null

  return <ValueList data={data} />
}

export const WithCustomMissingValue = () => {
  const data = makeValues(3)
  data[1].value = null

  return <ValueList data={data} missingValueText="not assigned" />
}

export default {
  component: ValueList,
  title: 'Editors/Value List',
}
