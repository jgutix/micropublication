import React from 'react'

import Author from '../../src/preview/Author'

export const Base = () => <Author affiliations={[1, 2]} name="John Smith" />

export const Corresponding = () => (
  <Author affiliations={[1, 2]} isCorresponding name="John Smith" />
)

export const Added = () => (
  <Author affiliations={[1, 2]} isAdded isCorresponding name="John Smith" />
)

export const Removed = () => (
  <Author affiliations={[1, 2]} isCorresponding isRemoved name="John Smith" />
)

export const Last = () => (
  <Author affiliations={[1, 2]} isLast name="John Smith" />
)

export default {
  component: Author,
  title: 'Preview/Author',
}
