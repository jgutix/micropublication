import React from 'react'
import { lorem } from 'faker'

import TextBlockList from '../../src/preview/TextBlockList'

export const Base = () => (
  <TextBlockList
    label={lorem.words(3)}
    values={[lorem.sentences(7), lorem.sentences(8)]}
  />
)

const sameValue = lorem.sentences(8)
const previousValues = [lorem.sentences(7), sameValue]
const newValues = [sameValue, lorem.sentences(7)]
const label = lorem.words(3)

export const WithDiff = () => (
  <TextBlockList
    label={label}
    previousValues={previousValues}
    values={newValues}
  />
)

WithDiff.story = {
  parameters: {
    docs: {
      storyDescription:
        'Providing the "previousValues" prop will trigger diffing. Please note that since this is an array of values, it diffs the whole block of text.',
    },
  },
}

export const HideRemovalsOnly = () => (
  <TextBlockList
    label={label}
    previousValues={previousValues}
    showRemoved={false}
    values={newValues}
  />
)

export const HideAllDiffs = () => (
  <TextBlockList
    label={label}
    previousValues={previousValues}
    showDiff={false}
    values={newValues}
  />
)

export default {
  component: TextBlockList,
  title: 'Preview/Text Block List',
}
