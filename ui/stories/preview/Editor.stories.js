import React from 'react'
import { lorem } from 'faker'

import Editor from '../../src/preview/Editor'

export const Base = () => <Editor value={lorem.sentence()} />

export const TitleVariant = () => (
  <Editor
    value={`<p>${lorem.sentence()}<sup>${lorem.word()}</sup></p>`}
    variant="title"
  />
)

export const CaptionVariant = () => (
  <Editor value={`<p>${lorem.sentences(8)}</p>`} variant="caption" />
)

export const MetadataVariant = () => (
  <Editor value={`<p>${lorem.sentence()}</p>`} variant="metadata" />
)

export const MetadataVariantWithLabel = () => (
  <Editor
    label={lorem.word()}
    value={`<p>${lorem.sentence()}</p>`}
    variant="metadata"
  />
)

export const WithDiff = () => (
  <Editor
    previousValue="<p>Rerum autem aliquid quis autem</p>"
    value="<p>Rerum autem aliquid quis consequatur</p>"
  />
)

export const WithDiffButHideRemoved = () => (
  <Editor
    previousValue="<p>Rerum autem aliquid quis autem</p>"
    showRemoved={false}
    value="<p>Rerum autem aliquid quis consequatur</p>"
  />
)

export const WithDiffButHideAll = () => (
  <Editor
    previousValue="<p>Rerum autem aliquid quis autem</p>"
    showDiff={false}
    value="<p>Rerum autem aliquid quis consequatur</p>"
  />
)

export const WholeTextWasAdded = () => (
  <Editor isAdded value="<p>Rerum autem aliquid quis consequatur</p>" />
)

export const WholeTextWasRemoved = () => (
  <Editor isRemoved value="<p>Rerum autem aliquid quis consequatur</p>" />
)

export default {
  component: Editor,
  title: 'Preview/Editor',
}
